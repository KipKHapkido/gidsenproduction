# README #

* Eindwerk voor de opleiding tot gegradueerde (HBO5) optie programmeren. Betreft een platform voor de gidsen/klanten van de Olmense Zoo.

### How do I get set up? ###

* Opbouw
Models, klasses en Business Logic werden in aparte libraries gezet 
* Afhankelijkheden
MySQL
* Database configuratie
Ondertussen draait het project onder MySQL ipv. SQL Server Express. Dit is nog niet zichtbaar in de Web.Config. Voor richtlijnen om ASP.NET MVC te koppelen met MySQL: neem gerust contact met me op.