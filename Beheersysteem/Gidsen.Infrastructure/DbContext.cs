﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using Gidsen.BOL.Models;

namespace Gidsen.DAL
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("GidsenProjectDB", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
        }

        public virtual DbSet<SteunProject> SteunProject { get; set; }
        public virtual DbSet<Vergadering> Vergadering { get; set; }
        public virtual DbSet<Agendapunt> Agendapunt { get; set; }
        public virtual DbSet<Verslag> Verslag { get; set; }
        public virtual DbSet<Vergoeding> Vergoeding { get; set; }
        public virtual DbSet<Categorie> Categorie { get; set; }
        public virtual DbSet<Dier> Dier { get; set; }
        public virtual DbSet<DierDocumentatie> DierDocumentatie { get; set; }
        public virtual DbSet<Gidsbeurt> Gidsbeurt { get; set; }
        public virtual DbSet<LeeftijdCategorie> LeeftijdCategorie { get; set; }
        public virtual DbSet<GidsbeurtUitvoerder> GidsbeurtUitvoerder { get; set; }
        public virtual DbSet<GidsRol> GidsRol { get; set; }
        public virtual DbSet<Thema> Thema { get; set; }
        public virtual DbSet<ThemaDocumentatie> ThemaDocumentatie { get; set; }
        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<MenuRol> MenuRol { get; set; }
        public virtual DbSet<IdentityUserRole> UserRole { get; set; }

        
    }
}
