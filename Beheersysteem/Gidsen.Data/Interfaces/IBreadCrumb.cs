﻿using System.Web.Mvc;

namespace Gidsen.BOL.Interfaces
{
    public interface IBreadCrumb
    {
        [HttpGet]
        PartialViewResult BreadCrumbs();
    }
}
