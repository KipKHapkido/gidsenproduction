﻿namespace Gidsen.BOL.Models
{
    public class Vergoeding: Zeus
    {
        public int Jaar { get; set; }

        public decimal Hoofdbedrag { get; set; }

        public decimal Steunproject { get; set; }
    }

    public class SteunProject : Zeus
    {
        public ApplicationUser User { get; set; }

        public int Jaartal { get; set; }

        public bool Steun { get; set; }
    }

    public class Uitkering
    {
        public ApplicationUser Gids { get; set; }

        public bool Steunproject { get; set; }

        public int UitgevoerdeGidsbeurten { get; set; }

        public int GevolgdeGidsbeurten { get; set; }
    }
}