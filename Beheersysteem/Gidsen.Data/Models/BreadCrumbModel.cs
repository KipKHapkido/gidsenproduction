﻿namespace Gidsen.BOL.Models
{
    public class BreadCrumb
    {
        public string Url { get; set; }

        public string Beschrijving { get; set; }
    }
}
