﻿namespace Gidsen.BOL.Models
{
    public class Message
    {
        public string Boodschap { get; set; }
        public MessageSeverity Severity { get; set; }

        public enum MessageSeverity
        {
            Success,
            Warning,
            Error
        }

        public string GetBootstrapStatus()
        {
            switch (Severity)
            {
                case MessageSeverity.Success:
                    return "success";
                case MessageSeverity.Warning:
                    return "warning";
            }
            return "danger";

        }

        public Message(string boodschap, MessageSeverity severity)
        {
            Boodschap = boodschap;
            Severity = severity;
        }

    }
}
