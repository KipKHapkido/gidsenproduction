﻿using System.Collections.Generic;

namespace Gidsen.BOL.Models
{
    public class Categorie: Zeus
    {
        public string Naam { get; set; }

        public List<Dier> Dier { get; set; }
    }

    public class Dier: Zeus
    {
        public string Naam { get; set; }

        public List<DierDocumentatie> DierDocumentatie { get; set; }

        public Categorie Categorie { get; set; }
    }

    public class DierDocumentatie: Documentatie
    {
        public Dier Dier { get; set; }
    }
}