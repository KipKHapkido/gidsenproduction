﻿using System;
using System.Collections.Generic;

namespace Gidsen.BOL.Models
{
    public class Vergadering: Zeus
    {
        public DateTime Datum { get; set; }

        public List<Agendapunt> Agendapunten { get; set; }

        public Verslag Verslag { get; set; }

        public bool VerslagAanwezig { get; set; }

    }

    public class Agendapunt: Zeus
    {
        public string Omschrijving { get; set; }

        public virtual ApplicationUser User { get; set; }

        public Vergadering Vergadering { get; set; }
    }

    public class Verslag: Documentatie
    {
        
    }
}