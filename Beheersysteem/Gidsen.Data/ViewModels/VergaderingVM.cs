﻿using Gidsen.BOL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class AgendaPuntVm : BaseVm
    {
        [Required(ErrorMessage = "Omschrijving agendapunt is een verplicht veld!")]
        [Display(Name = "Omschrijving?")]
        public string Omschrijving { get; set; }

        public int VergaderingId { get; set; }

        public string UserId { get; set; }

    }

    public class AgendaPuntListVm : BaseVm
    {
        public List<Agendapunt> AgendaPuntList { get; set; }

        public DateTime Wanneer { get; set; }

        public AgendaPuntListVm()
        {
            AgendaPuntList = new List<Agendapunt>();
        }
    }

    public class VergaderingVm: BaseVm
    {
        [Required(ErrorMessage = "Datum&Tijdstip is een verplicht veld!")]
        [Display(Name = "Datum & Tijdstip?")]
        public DateTime Datum { get; set; }
    }

    public class VergaderingListVm : BaseVm
    {
        public List<Vergadering> VergaderingLijst { get; set; }
    }
}
