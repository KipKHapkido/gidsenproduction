﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Gidsen.BOL.Models;

namespace Gidsen.BOL.ViewModels
{
    public class CompleteUserVm: BaseVm
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Gebruikersnaam is een verplicht veld!")]
        [Display(Name = "Gebruikersnaam")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Familienaam is een verplicht veld!")]
        public string Familienaam { get; set; }

        [Required(ErrorMessage = "Voornaam is een verplicht veld!")]
        public string Voornaam { get; set; }

        [Required(ErrorMessage = "Email is een verplicht veld!")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefoonnummer is een verplicht veld!")]
        [Display(Name = "Telefoonnummer")]
        public string PhoneNumber { get; set; }

        public List<IdentityRole> Roles { get; set; }

        public bool SteunProject { get; set; }

        public string RoleId { get; set; }

        public string HuidigeRolnaam { get; set; }
    }

    public class CompleteUserListVm : BaseVm
    {
        public List<ApplicationUser> UserList { get; set; }

        public List<IdentityRole> Roles { get; set; }

        public CompleteUserListVm()
        {
            UserList = new List<ApplicationUser>();
        }

    }
}
