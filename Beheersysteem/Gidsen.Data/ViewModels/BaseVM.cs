﻿using System.Collections.Generic;
using Gidsen.BOL.Models;
using static Gidsen.BOL.Models.Message;

namespace Gidsen.BOL.ViewModels
{
    public abstract class BaseVm
    {
        public int Id { get; set; }

        public List<Message> ListMessage { get; set; }
        
        public List<Message> GetListMessage()
        {
            return ListMessage;
        }

        protected BaseVm()
        {
            ListMessage = new List<Message>();
        }

        public void AddMessage(string message, MessageSeverity severity)
        {
            ListMessage.Add(new Message(message, severity));
        }
    }
}
