﻿using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class DierVm : BaseVm
    {
        public List<Categorie> CategorieLijst { get; set; }

        [Required(ErrorMessage = "Naam is een verplicht veld!")]
        public string Naam { get; set; }

        [Required(ErrorMessage = "Categorie is een verplicht veld!")]
        public int CategorieId { get; set; }

        public Categorie Categorie { get; set; }
    }

    public class DierLijstVm : BaseVm
    {
        public List<Dier> DierLijst { get; set; }
    }

    
}
