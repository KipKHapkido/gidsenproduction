﻿using System.Collections.Generic;
using Gidsen.BOL.Models;

namespace Gidsen.BOL.ViewModels
{
    public class DocumentatieVm: BaseVm
    {
        public List<Documentatie> DocLijst { get; set; }

        public DocumentatieVm()
        {
            DocLijst = new List<Documentatie>();
        }
    }
}
