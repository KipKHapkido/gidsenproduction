﻿using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class CategorieVm: BaseVm
    {
        [Required(ErrorMessage = "Naam is een verplicht veld!")]
        public string Naam { get; set; }
    }

    public class CategorieListVm : BaseVm
    {
        public List<Categorie> CategorieLijst { get; set; }
    }
}
