﻿using Gidsen.BOL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class MenuItemsVm: BaseVm
    {
        [Required]
        public string Weergave { get; set; }

        [Required]
        public string MenuString { get; set; }

        [Required]
        [Display(Name ="Gekoppelde gebruikersrollen?")]
        public List<string> RoleId { get; set; }

        [Display(Name = "Top-Navigatie?")]
        public bool TopNavigatie { get; set; }

        public List<IdentityRole> Roles { get; set; }

        
        public MenuRol MenuRol { get; set; }

        public MenuItemsVm()
        {
            
            Roles = new List<IdentityRole>();
        }
    }

    public class MenuItemsListVm : BaseVm
    {
        public List<Menu> MenuList { get; set; }

        public MenuItemsListVm()
        {
            MenuList = new List<Menu>();
        }
    }
}
