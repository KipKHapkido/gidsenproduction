﻿using System.Web.Optimization;

namespace Beheersysteem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        "~/Scripts/jquery.cookie.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-print").Include(
                        "~/Scripts/jquery.print.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/uploadifive").Include(
                        "~/Scripts/jquery.uploadifive.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/moment-with-locales.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/bootbox.js",
                      "~/Scripts/bootstrap-toggle.js",
                      "~/Scripts/bootstrap-slider.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-datetimepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/dhtmlxscheduler").Include(
                    "~/Scripts/dhtmlxscheduler/dhtmlxscheduler.js",
                    "~/Scripts/dhtmlxscheduler/locale/locale_nl.js"));

            bundles.Add(new ScriptBundle("~/bundles/ekko-lightbox").Include(
                   "~/Scripts/ekko-lightbox.js"));

            bundles.Add(new ScriptBundle("~/bundles/agenda").Include(
                   "~/Scripts/agenda.js"));
                     
            bundles.Add(new ScriptBundle("~/bundles/general-reload").Include(
                  "~/Scripts/general-reload.js"));

            bundles.Add(new ScriptBundle("~/bundles/year-reload").Include(
                  "~/Scripts/year-reload.js"));

            bundles.Add(new ScriptBundle("~/bundles/role-reload").Include(
                  "~/Scripts/role-reload.js"));

            bundles.Add(new ScriptBundle("~/bundles/crud").Include(
                  "~/Scripts/crud.js"));

            bundles.Add(new ScriptBundle("~/bundles/crud-agendapunt").Include(
                  "~/Scripts/crud-agendapunt.js"));

            bundles.Add(new ScriptBundle("~/bundles/doc").Include(
                  "~/Scripts/documentatie.js"));

            bundles.Add(new StyleBundle("~/Styles/doc").Include(
                    "~/Content/documentatie.css"));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-toggle.css",
                       "~/Content/bootstrap-slider.css",
                      "~/Content/bootstrap-datetimepicker.css"
                      ));

            bundles.Add(new StyleBundle("~/Styles/custom").Include(
                "~/Content/theme.css",
                "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Styles/ekko-lightbox").Include(
                    "~/Content/ekko-lightbox.css"));

            bundles.Add(new StyleBundle("~/Content/uploadifive").Include(
                    "~/Content/uploadifive.css"));

            bundles.Add(new StyleBundle("~/Styles/tooltip").Include(
                "~/Content/tooltip.css"));

        }
    }
}
