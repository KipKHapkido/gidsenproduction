﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Interfaces;
using System;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class DierController : Controller, ICrudGeneric<int, DierVm>, IList, IBreadCrumb
    {
        private readonly DierLogic _logic = new DierLogic();

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_dierList", _logic.GetAllDieren());
        }

        public PartialViewResult Create()
        {
            ViewBag.Title = "create";
            return PartialView("_createUpdate", _logic.GetAllActiveCategories());
        }

        public PartialViewResult Edit(int id)
        {
            ViewBag.Title = "edit";
            return PartialView("_createUpdate", _logic.GetDierById(id));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(DierVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateDier(model);
                return Json(true);
            }
            DierVm d_VM = null;
            if (model.Id == 0)
            {
                d_VM = _logic.GetAllActiveCategories();
                model.CategorieLijst = d_VM.CategorieLijst;
                ViewBag.Title = "create";
            }
            else
            {
                d_VM = _logic.GetDierById(model.Id);
                model.Categorie = d_VM.Categorie;
                ViewBag.Title = "edit";
            }

            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dier dier = _logic.ToggleActive(id);

            return Json(dier != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/DierenBeheer", Beschrijving = "Beheer dieren"},
                new BreadCrumb{ Url = "/Beheerder/Dier", Beschrijving = "Dier"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}
