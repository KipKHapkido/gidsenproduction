﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class LeeftijdCategorieController : Controller, ICrudGeneric<int, LeeftijdCategorieVm>, IList, IBreadCrumb
    {
        private readonly LeeftijdCategorieLogic _logic = new LeeftijdCategorieLogic();

        // GET: Beheerder/LeeftijdCategorie
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_leeftijdcategorieList", _logic.GetAllLeeftijdCategorie());
        }

        // GET: Beheerder/LeeftijdCategorie/Create
        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", new LeeftijdCategorieVm());
        }

        // GET: Beheerder/LeeftijdCategorie/Edit 
        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetLeeftijdCategorieById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(LeeftijdCategorieVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateLeeftijdCategorie(model);
                return Json(true);
            }
            
            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeeftijdCategorie leeftijdCategorie = _logic.ToggleActive(id);

            return Json(leeftijdCategorie != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Leeftijdcategorie", Beschrijving = "Leeftijdcategorie"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}
