﻿using System.Web.Mvc;
using Gidsen.BOL.Interfaces;
using Gidsen.BOL.Models;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class DierenBeheerController : Controller, IBreadCrumb
    {
        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/DierenBeheer", Beschrijving = "Beheer dieren"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }

        // GET: Beheerder/DierenBeheer
        public ActionResult Index()
        {
            return View();
        }
    }
}