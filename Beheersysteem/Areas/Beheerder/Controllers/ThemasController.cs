﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class ThemasController : Controller, ICrudGeneric<int, ThemaVm>, IList, IBreadCrumb
    {
        private readonly ThemaLogic _logic = new ThemaLogic();

        // GET: Beheerder/Themas
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_themaList", _logic.GetAllThemas());
        }

        // GET: Beheerder/Themas/Create
        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", new ThemaVm());
        }

        // GET: Beheerder/Themas/Edit/5
        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetThemaById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(ThemaVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateThema(model);
                return Json(true);
            }

            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thema thema = _logic.ToggleActive(id);

            return Json(thema != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Themas", Beschrijving = "Thema"}
                
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}
