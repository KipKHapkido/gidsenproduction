﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    public class GebruikersController : Controller, ICrudGeneric<string, CompleteUserVm>, IListRol, IBreadCrumb
    {
        private readonly GebruikersLogic _logic;

        public GebruikersController()
        {
            _logic = new GebruikersLogic();
        }

        // GET: Beheerder/Gebruikers
        [Authorize(Roles = "Admin,Beheerder")]
        public ActionResult Index()
        {
            return View(_logic.GetRoles(User));
        }

        [Authorize]
        public PartialViewResult List(string rol)
        {
            CompleteUserListVm culVm;
            if (string.IsNullOrEmpty(rol))
            {
                culVm = _logic.GetUserData(User);
                ViewBag.Soort = "Individueel";
            }
            else
            {
                culVm = _logic.GetAllUsers(rol);
                ViewBag.Soort = "GebruikersLijst";
            }
            
            return PartialView("_gebruikersList", culVm);
        }

        // GET: Beheerder/Gebruikers/Create
        [Authorize(Roles = "Admin,Beheerder")]
        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", _logic.GetRoles(User));
        }

        // GET: Beheerder/Gebruikers/Edit 
        [Authorize]
        public PartialViewResult Edit(string id)
        {
            return PartialView("_createUpdate", _logic.GetUserDataById(id, User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateUpdate(CompleteUserVm model)
        {
            if (ModelState.IsValid)
            {
                bool gelukt = _logic.AddOrUpdateGebruikerAsync(model, Server);
                return Json(gelukt);
            }
            CompleteUserVm cuVm = _logic.GetRoles(User);
            model.Roles = cuVm.Roles;
            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Beheerder")]
        public ActionResult ToggleActive(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser gebruiker = _logic.ToggleActive(id);

            return Json(gebruiker != null);
        }

        [Authorize(Roles = "Admin,Beheerder")]
        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Gebruikers", Beschrijving = "Gebruikers"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }

        [HttpGet]
        public PartialViewResult SteunProject(string id)
        {
            return PartialView("_steunProject", _logic.GetSteun(id));
        } 
    }
}
