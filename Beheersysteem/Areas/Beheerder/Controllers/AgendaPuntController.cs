﻿using Gidsen.BOL.Interfaces;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder,Gids,Stagiair")]
    public class AgendaPuntController : Controller, IBreadCrumb, IList, ICrudGeneric<int, AgendaPuntVm>
    {
        private readonly VergaderingLogic _logic = new VergaderingLogic();

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>();
            if (User.IsInRole("Admin") || User.IsInRole("Beheerder"))
            {
                breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/VergaderingBeheer", Beschrijving = "Vergadering-beheer" });
            }
            breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/AgendaPunt", Beschrijving = "Agendapunten" });

            ViewBag.Soort = "Vergadering";
            
            return PartialView("_breadCrumbs", breadCrumbList);
        }

        // GET: Beheerder/AgendaPunt
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? vergaderingId)
        {
            return PartialView("_agendaPuntList", _logic.GetAllAgendaPuntenByAgendaId(vergaderingId));
        }

        public PartialViewResult ListAgendaPunten(int? jaar)
        {
            return PartialView("_vergaderingList", _logic.GetAllVergaderingen(jaar));
        }

        public PartialViewResult Create()
        {
            int vergaderingId = int.Parse(Request.QueryString["vergaderingId"]);
            return PartialView("_createUpdate", new AgendaPuntVm { VergaderingId = vergaderingId, UserId = User.Identity.GetUserId() });
        }

        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetAgendaPuntById(id));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(AgendaPuntVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateAgendaPunt(model);
                return Json(true);
            }

            return PartialView("_createUpdate", model);
        }

        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agendapunt agendapunt = _logic.ToggleActiveAgendapunt(id);

            if (agendapunt == null)
            {
                return Json(false);
            }
            return Json(true);
        }
    }
}