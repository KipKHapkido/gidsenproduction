﻿using Gidsen.BLL.Logic;
using Gidsen.BLL.Tools;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Web;
using System.Web.Mvc;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder,Gids,Stagiair")]
    public class ThemaDocumentatieController : Controller, IBreadCrumb
    {
        private readonly ThemaLogic _tLogic = new ThemaLogic();
        private readonly ThemaDocumentatieLogic _tdLogic = new ThemaDocumentatieLogic();
        private readonly DocTool _doc = new DocTool();

        // GET: Beheerder/ThemaDocumentatie
        public ActionResult Overzicht()
        {
            ThemaListVm tVm = _tLogic.GetAllActiveThema();
            
            return View(tVm);
        }

        public PartialViewResult Lijst(int id)
        {
            DocumentatieVm dVm = _tdLogic.GetDocListByThemaId(id);
                        
            return PartialView("_docLijst", dVm);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, int id)
        {
            string fileName = _doc.BestandUploaden(file, Server);
            
            if (!string.IsNullOrEmpty(fileName))
            { 
                _tdLogic.AddNewDocsToDb(fileName, id);
                return Json(true);
            }
            return Json(false);

        }

        [HttpPost]
        public ActionResult DeleteFile(int id)
        {
            ThemaDocumentatie doc = _tdLogic.GetFileById(id);
            string bestandsnaam = doc.Bestandsnaam;

            bool gelukt = _doc.BestandVerwijderen(bestandsnaam, Server);
            if (gelukt)
            {
                _tdLogic.RemoveDocFromDb(id);
                return Json(true);
            }
            return Json(false);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>();
            if (User.IsInRole("Admin") || User.IsInRole("Beheerder"))
            {
                breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/Themas", Beschrijving = "Thema" });
            }
            
            breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/ThemaDocumentatie/Overzicht", Beschrijving = "Documentatie thema's" });            

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}