﻿using Gidsen.BLL.Logic;
using System.Web.Mvc;

namespace Beheersysteem.Areas.Algemeen.Controllers
{
    public class HomeController : Controller
    {
        private readonly MenuLogic _logic = new MenuLogic();

        [Authorize]
        public PartialViewResult MenuLijst()
        {
            return PartialView("_MenuPartial", _logic.GetDynamicMenuItems(User, true));
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}