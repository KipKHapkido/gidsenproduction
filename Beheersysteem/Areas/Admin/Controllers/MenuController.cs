﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Beheersysteem.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MenuController : Controller, ICrudGeneric<int, MenuItemsVm>, IList, IBreadCrumb
    {
        private readonly MenuLogic _logic = new MenuLogic();

        // GET: Admin/Menu
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_menuList", _logic.GetAllMenuItems());
        }

        // GET: Admin/Menu/Create
        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", _logic.GetRoles());
        }

        // GET: Admin/Menu/Edit/5
        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetMenuItemById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(MenuItemsVm model)
        {
            if (ModelState.IsValid && Regex.Matches(model.MenuString, "/").Count == 2)
            {
                Menu menu = null;
                menu = model.Id == 0 ? _logic.AddMenuItem(model) : _logic.UpdateMenuItem(model);
                if (menu != null)
                {
                    return Json(true);
                }
            }
            MenuItemsVm miVM = _logic.GetRoles();
            model.Roles = miVM.Roles;
            if (model.MenuString != null)
            {
                if (Regex.Matches(model.MenuString, "/").Count != 2)
                {
                    model.AddMessage("MenuString wordt als volgt opgebouwd: Area/Controller/Action !", Message.MessageSeverity.Error);
                }
            }
            
            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = _logic.ToggleActive(id);

            if (menu == null)
            {
                return Json(false);
            }
            return Json(true);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Admin/Menu", Beschrijving = "Menu's"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}
