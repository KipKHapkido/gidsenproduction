﻿$.ajax({
    url: controller + "BreadCrumbs",
    async: true,
    type: "GET",
    success: function (resp) {
        $("#breadcrumb").html(resp);
    }
});


$.fn.vergadering = function () {
    $(".vergaderingLijst").html(spinner);
    var dataString = "jaar=" + $("#jaartal option:selected").val();

    $.ajax({
        url: controller + "ListAgendaPunten",
        async: true,
        type: "GET",
        data: dataString,
        success: function (resp) {
            $(".vergaderingLijst").html(resp);

            $.fn.removeMessage();
            $.fn.tooltips();

        }
    });
}

$(document).on("change", "#jaartal", function () {
    $.fn.vergadering();
});

$.fn.reload = function () {
    $(".overzicht").html(spinner);
    var dataString = "vergaderingId=" + $("#VergaderingId option:selected").val();

    $.ajax({
        url: controller + "List",
        async: true,
        type: "GET",
        data: dataString,
        success: function (resp) {
            $(".overzicht").html(resp);

            $.fn.removeMessage();
            $.fn.tooltips();

        }
    });
}

$(document).on("change", "#VergaderingId", function () {
    $.fn.reload();
});

$(document).on("click", ".new", function (e) {
    e.preventDefault();
    var dataString = "vergaderingId=" + $("#VergaderingId option:selected").val();

    $.ajax({
        url: controller + "Create",
        async: true,
        data: dataString,
        type: "GET",
        success: function (resp) {
            $("#myModal .modal-body").html(resp);
            $("#myModal .modal-title").html(titel + "aanmaken");

            $("#submit").val("Aanmaken");
            $("#myModal").modal("show");
            $.fn.removeMessage();
        }
    });
});

$(document).on("submit", "#CreateUpdate", function (e) {
    e.preventDefault();

    var dataString = $("#CreateUpdate").serialize();
    $.ajax({
        url: controller + "CreateUpdate",
        async: true,
        data: dataString,
        type: "POST",
        success: function (resp) {
            if (resp === true) {
                $("#myModal").modal("hide");
                $.fn.reload();
                $("<div class='alert alert-success'>Toevoegen/Wijzigen gelukt!</div>").insertBefore(".overzicht");

            } else {
                $(".modal-body").html(resp);
                $("#TopNavigatie").bootstrapToggle();
            }

        }
    });

});

$(document).on("click", ".edit", function (e) {
    e.preventDefault();
    var dataString = "id=" + $(this).attr("data-value");

    $.ajax({
        url: controller + 'Edit',
        async: true,
        data: dataString,
        type: "GET",
        success: function (resp) {
            $("#myModal .modal-body").html(resp);
            $("#myModal .modal-title").html(titel + "wijzigen");
            $("#submit").val("Wijzigen");
            $("#myModal").modal('show');

            $.fn.removeMessage();
        }
    });
});

$(document).on("click", ".toggle-active", function () {
    var dataString = "id=" + $(this).val();
    bootbox.confirm("Bent u zeker dat u dit item wil (de)activeren?", function (result) {
        if (result) {
            $.ajax({
                url: controller + 'ToggleActive',
                async: true,
                data: dataString,
                type: "POST",
                success: function () {
                    $.fn.reload();

                    $("<div class='alert alert-success'>(de)activeren gelukt!</div>").insertBefore(".overzicht");
                }
            });
        }
    });
});

$(document).on("click", ".print", function () {
    $(".lijstagendapunten").print({
        globalStyles: true,
        mediaPrint: false,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred(),
        timeout: 750,
        title: null,
        doctype: '<!doctype html>'
    });
});