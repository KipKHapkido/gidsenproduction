﻿$.fn.reload = function () {
    $(".overzicht").html(spinner);
    var dataString = "jaar=" + $("#jaartal option:selected").val();

    $.ajax({
        url: controller + "List",
        async: true,
        type: "GET",
        data: dataString,
        success: function (resp) {
            $(".overzicht").html(resp);

            $.fn.removeMessage();
            $.fn.tooltips();

        }
    });
}

function reload() {
    $(".overzicht").html(spinner);
    var dataString = "jaar=" + $("#jaartal option:selected").val();

    $.ajax({
        url: controller + "List",
        async: true,
        type: "GET",
        data: dataString,
        success: function (resp) {
            $(".overzicht").html(resp);

            $.fn.removeMessage();
            $.fn.tooltips();

        }
    });
}

$(document).on("change", "#jaartal", function () {
    $.fn.reload();
});


