﻿using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Gidsen.BLL.Logic
{
    public class ThemaLogic: BaseLogic
    {
        private readonly ThemaListVm _tlVm;
        private readonly ThemaVm _tVm;

        public ThemaLogic()
        {
            _tlVm = new ThemaListVm();
            _tVm = new ThemaVm();
        }

        public ThemaListVm GetAllActiveThema()
        {
            List<Thema> themaLijst = Db.Thema.Where(x => x.Actief).ToList();
            if (themaLijst.Any())
            {
                _tlVm.ThemaLijst = themaLijst;
            } else
            {
                _tlVm.AddMessage("Er werden geen resultaten teruggevonden voor uw bevraging!", Message.MessageSeverity.Error);
            }
            return _tlVm;
        }

        public ThemaListVm GetAllThemas()
        {
            List<Thema> themaLijst = Db.Thema.ToList();
            if (themaLijst.Any())
            {
                _tlVm.ThemaLijst = themaLijst;
            }
            else
            {
                _tlVm.AddMessage("Er werden geen resultaten teruggevonden voor uw bevraging!", Message.MessageSeverity.Error);
            }
            return _tlVm;
        }

        public ThemaVm GetThemaById(int id)
        {
            Thema thema = Db.Thema.FirstOrDefault(x => x.Id == id);
            if (thema != null)
            {
                _tVm.Id = thema.Id;
                _tVm.Naam = thema.Naam;
            }
            else
            {
                _tVm.AddMessage("Er werden geen resultaten teruggevonden voor uw bevraging!", Message.MessageSeverity.Error);
            }
            return _tVm;
        }

        public void AddOrUpdateThema(ThemaVm model)
        {
            Thema thema = new Thema
            {
                Id = model.Id,
                Naam = model.Naam
            };

            Db.Entry(thema).State = thema.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public Thema ToggleActive(int id)
        {
            Thema thema = Db.Thema.FirstOrDefault(x => x.Id == id);
            if (thema == null) return null;

            thema.Actief = !thema.Actief;
            Db.Entry(thema).State = EntityState.Modified;
            Db.SaveChanges();
            return thema;
        }
    }
}
