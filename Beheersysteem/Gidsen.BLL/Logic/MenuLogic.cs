﻿using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Security.Principal;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;

namespace Gidsen.BLL.Logic
{
    public class MenuLogic : BaseLogic
    {
       private readonly MenuItemsVm _miVm;
       private readonly MenuItemsListVm _milVm;


        public MenuLogic()
        {
            //dm_VM = new DynamicMenuVM();
            _miVm = new MenuItemsVm();
            _milVm = new MenuItemsListVm();
        }

        public MenuItemsListVm GetAllMenuItems()
        {
            List<Menu> menuList = Db.Menu
                .OrderByDescending(x => x.Actief)
                .ThenBy(x => x.Area)
                .ThenBy(x => x.Controller)
                .ThenBy(x => x.Action)
                .ToList();

            if (menuList.Any())
            {
                _milVm.MenuList = menuList;
            }
            else
            {
                _milVm.AddMessage("Er ging iets mis. Er konden geen menu-items teruggevonden worden.", Message.MessageSeverity.Error);
            }
            return _milVm;
        }

        public MenuItemsListVm GetDynamicMenuItems(IPrincipal user, bool topNavigatie)
        {
            if (user.Identity.IsAuthenticated)
            {
                ApplicationUser gebruiker = Db.Users.Find(user.Identity.GetUserId());
                IdentityUserRole rol = gebruiker.Roles.FirstOrDefault();

                var qry =
                    from menu in Db.Menu
                    join menuRol in Db.MenuRol on menu.Id equals menuRol.Menu.Id
                    where menuRol.IdentityRole.Id.Equals(rol.RoleId) && menu.Actief && menu.TopNavigatie == topNavigatie
                    orderby menu.Id
                    select menu;

                List<Menu> menuLijst = qry.ToList();

                _milVm.MenuList = menuLijst;
            }
            return _milVm;
        }


        public MenuItemsVm GetRoles()
        {
            _miVm.Roles = Db.Roles.ToList();
            _miVm.AddMessage("De menustring wordt als volgt opgebouwd: Area/Controller/Action", Message.MessageSeverity.Warning);
            return _miVm;
        }


        public MenuItemsVm GetMenuItemById(int id)
        {
            Menu item = Db.Menu.FirstOrDefault(x => x.Id == id);

            _miVm.Id = id;
            if (item != null)
            {
                _miVm.Weergave = item.Weergave;
                _miVm.MenuString = item.Area + "/" + item.Controller + "/" + item.Action;
                _miVm.TopNavigatie = item.TopNavigatie;
            }

            _miVm.Roles = Db.Roles.ToList();

            List<string> roleId = Db.MenuRol.Where(x => x.Menu.Id == id).Select(x => x.IdentityRole.Id).ToList();
            if (roleId.Any())
            {
                _miVm.RoleId = roleId;
            }

            return _miVm;
        }


        public Menu AddMenuItem(MenuItemsVm model)
        {
            Menu menu = AddOrUpdateMenuItem(model);

            if (menu != null)
            {
                AddMenuRollen(model, menu);
            }

            return menu;
        }

        public Menu UpdateMenuItem(MenuItemsVm model)
        {
            Menu menu = AddOrUpdateMenuItem(model);

            if (menu != null)
            {
                Db.MenuRol.RemoveRange(Db.MenuRol.Where(x => x.Menu.Id == menu.Id));
                Db.SaveChanges();

                AddMenuRollen(model, menu);
            }

            return menu;
        }

        public Menu ToggleActive(int id)
        {
            Menu menu = Db.Menu.FirstOrDefault(x => x.Id == id);
            if (menu != null)
            {
                menu.Actief = !menu.Actief;
                Db.Entry(menu).State = EntityState.Modified;
                Db.SaveChanges();
            }
            return menu;
        }

        public Menu AddOrUpdateMenuItem(MenuItemsVm model)
        {
            Menu menu = null;
            List<string> menuStrings = model.MenuString.Split('/').ToList();
            
            if (menuStrings.Count == 3)
            {
                menu = new Menu
                {
                    Id = model.Id,
                    Weergave = model.Weergave,
                    Area = menuStrings.ElementAt(0),
                    Controller = menuStrings.ElementAt(1),
                    Action = menuStrings.ElementAt(2),
                    TopNavigatie = model.TopNavigatie
                };

                Db.Entry(menu).State = menu.Id == 0 ?
                    EntityState.Added :
                    EntityState.Modified;

                Db.SaveChanges();
            }

            return menu;
        }

        private void AddMenuRollen(MenuItemsVm model, Menu menu)
        {
            List<MenuRol> rolLijst = new List<MenuRol>();
            foreach (var rolId in model.RoleId)
            {
                rolLijst.Add(new MenuRol
                {
                    Actief = true,
                    Menu = menu,
                    IdentityRole = Db.Roles.Find(rolId)
                });
            }
            Db.MenuRol.AddRange(rolLijst);
            Db.SaveChanges();

        }


    }
}
