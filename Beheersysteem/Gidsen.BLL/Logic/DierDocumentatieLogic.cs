﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Models;

namespace Gidsen.BLL.Logic
{
    public class DierDocumentatieLogic: BaseLogic
    {
        private readonly DocumentatieVm _dVm;
        private readonly DierLijstVm _dlVm;

        public DierDocumentatieLogic()
        {
            _dVm = new DocumentatieVm();
            _dlVm = new DierLijstVm();
        }
                
        public DierLijstVm GetAllActiveAnimals()
        {
            List<Dier> dierLijst = Db.Dier
                .Include(y => y.Categorie)
                .Where(x => x.Actief)
                .OrderBy(y => y.Categorie.Naam)
                .ThenBy(z => z.Naam)
                .ToList();

            if (dierLijst.Any())
            {
                _dlVm.DierLijst = dierLijst;
            }
            else
            {
                _dlVm.AddMessage("Er ging iets mis. Er werden geen dieren teruggevonden.", Message.MessageSeverity.Error);
            }

            return _dlVm;
        }

        public DocumentatieVm GetDocListByDierId(int id)
        {
            List<DierDocumentatie> docList = Db.DierDocumentatie
                .Where(x => x.Dier.Id == id)
                .OrderBy(x => x.Bestandsnaam)
                .ToList();

            if (docList.Any())
            {
                _dVm.DocLijst = docList.ConvertAll(x => (Documentatie)x);
            }
            else
            {
                _dVm.AddMessage("Er werd geen documentatie teruggevonden voor het geselecteerde dier!!", Message.MessageSeverity.Error);
            }
            
            return _dVm;
        }
             
        public void AddNewDocsToDb(string fileName, int id)
        {
            string extensie = fileName.Substring(fileName.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();

            bool afbeelding = !extensie.Equals("pdf");

            Dier dier = Db.Dier.SingleOrDefault(x => x.Id == id);
            DierDocumentatie doc = new DierDocumentatie
            {
                Bestandsnaam = fileName,
                Dier = dier,
                Afbeelding = afbeelding
            };
            Db.DierDocumentatie.Add(doc);
            Db.SaveChanges();
        }

        public DierDocumentatie GetFileById(int id)
        {
            return (Db.DierDocumentatie.Find(id));
        }

        public void RemoveDocFromDb(int id)
        {
            var dierDoc = Db.DierDocumentatie.FirstOrDefault(x => x.Id == id);
            if (dierDoc != null)
            {
                Db.DierDocumentatie.Remove(dierDoc);
                Db.SaveChanges();
            }
            
        }
    }
}
