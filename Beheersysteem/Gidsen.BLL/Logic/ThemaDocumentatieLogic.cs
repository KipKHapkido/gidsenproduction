﻿using System;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Gidsen.BLL.Logic
{
    public class ThemaDocumentatieLogic: BaseLogic
    {
        private readonly DocumentatieVm _dVm;

        public ThemaDocumentatieLogic()
        {
            _dVm = new DocumentatieVm();
        }

        public DocumentatieVm GetDocListByThemaId(int id)
        {
            List<ThemaDocumentatie> docList = Db.ThemaDocumentatie.Where(x => x.Thema.Id.Equals(id)).OrderBy(y => y.Actief).ToList();
            if (docList.Any()) {
                _dVm.DocLijst = docList.ConvertAll(x => (Documentatie)x);
            } else
            {
                _dVm.AddMessage("Er werd geen documentatie teruggevonden voor het geselecteerde thema!", Message.MessageSeverity.Error);
            }

            return _dVm;
        }

        public void AddNewDocsToDb(string fileName, int id)
        {
            string extensie = fileName.Substring(fileName.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();

            bool afbeelding = !extensie.Equals("pdf");

            Thema thema = Db.Thema.FirstOrDefault(x => x.Id == id);
            ThemaDocumentatie doc = new ThemaDocumentatie()
            {
                Bestandsnaam = fileName,
                Thema = thema,
                Afbeelding = afbeelding
            };
            Db.ThemaDocumentatie.Add(doc);
            Db.SaveChanges();
            
        }

        public ThemaDocumentatie GetFileById(int id)
        {
            return (Db.ThemaDocumentatie.Find(id));
        }

        public void RemoveDocFromDb(int id)
        {
            var themaDoc = Db.ThemaDocumentatie.FirstOrDefault(x => x.Id == id);
            if (themaDoc != null)
            {
                Db.ThemaDocumentatie.Remove(themaDoc);
                Db.SaveChanges();
            }          
        }
    }
}
