﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;

namespace Gidsen.BLL.Logic
{
    public class VergaderingLogic : BaseLogic
    {
        private readonly VergaderingVm _vVm;
        private readonly VergaderingListVm _vlVm;
        private readonly AgendaPuntVm _apVm;
        private readonly AgendaPuntListVm _aplVm;

        public VergaderingLogic()
        {
            _vVm = new VergaderingVm();
            _vlVm = new VergaderingListVm();
            _apVm = new AgendaPuntVm();
            _aplVm = new AgendaPuntListVm();
        }

        public VergaderingListVm GetAllVergaderingen(int? jaar)
        {
            if (jaar != null)
            {
                List<Vergadering> vergaderingLijst = Db.Vergadering
                    .Include(x => x.Verslag)
                    .Where(x => x.Datum.Year == jaar && x.Actief).ToList();

                if (vergaderingLijst.Any())
                {
                    _vlVm.VergaderingLijst = vergaderingLijst;
                }
                else
                {
                    _vlVm.AddMessage("Er werden nog geen vergaderingen vastgelegd", Message.MessageSeverity.Error);
                }
            }
            else
            {
                _vlVm.AddMessage("U moet eerst een geldig jaar kiezen", Message.MessageSeverity.Error);
            }

            return _vlVm;
        }

        public AgendaPuntListVm GetAllAgendaPuntenByAgendaId(int? vergaderingId)
        {
            if (vergaderingId != null)
            {
                List<Agendapunt> agendaPuntLijst = Db.Agendapunt.Where(x => x.Vergadering.Id == vergaderingId && x.Actief).ToList();
                _aplVm.Wanneer = Db.Vergadering.Where(x => x.Id == vergaderingId).Select(x => x.Datum).SingleOrDefault();

                if (agendaPuntLijst.Any()) 
                {
                    _aplVm.AgendaPuntList = agendaPuntLijst;
                }
                else
                {
                    _aplVm.AddMessage("Er werden geen agendapunten teruggevonden voor de gekozen vergadering!", Message.MessageSeverity.Error);
                }
            }
            else
            {
                _aplVm.AddMessage("Gelieve een geldige keuze te maken uit de bestaande agendapunten!", Message.MessageSeverity.Error);
            }

            return _aplVm;
        }

        public void AddOrUpdateAgendaPunt(AgendaPuntVm model)
        {
            Agendapunt agendapunt = new Agendapunt
            {
                Id = model.Id,
                Omschrijving = model.Omschrijving,
                User = Db.Users.Find(model.UserId),
                Vergadering = Db.Vergadering.Find(model.VergaderingId)
            };

            Db.Entry(agendapunt).State = agendapunt.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public AgendaPuntVm GetAgendaPuntById(int id)
        {
            Agendapunt agendapunt = Db.Agendapunt
                .Include(x => x.User)
                .Include(x => x.Vergadering)
                .SingleOrDefault(x => x.Id == id);

            if (agendapunt != null)
            {
                _apVm.Id = agendapunt.Id;
                _apVm.Omschrijving = agendapunt.Omschrijving;
                _apVm.UserId = agendapunt.User.Id;
                _apVm.VergaderingId = agendapunt.Vergadering.Id;
            }
            else
            {
                _apVm.AddMessage("Er ging iets mis. De gegevens konden niet teruggevonden worden!", Message.MessageSeverity.Error);
            }

            return _apVm;
        }    

        public void AddOrUpdateVergadering(VergaderingVm model)
        {
            Vergadering vergadering = new Vergadering
            {
                Id = model.Id,
                Datum = model.Datum
            };

            Db.Entry(vergadering).State = vergadering.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public VergaderingVm GetVergaderingById(int id)
        {
            Vergadering vergadering = Db.Vergadering.FirstOrDefault(x => x.Id == id);
            if (vergadering != null)
            {
                _vVm.Id = vergadering.Id;
                _vVm.Datum = vergadering.Datum;
            }
            else
            {
                _vVm.AddMessage("Er ging iets mis. De gevraagde vergadering werd niet teruggevonden", Message.MessageSeverity.Error);
            }
            return _vVm;
        }

        public Vergadering ToggleActive(int id)
        {
            Vergadering vergadering = Db.Vergadering.SingleOrDefault(x => x.Id == id);
            if (vergadering == null) return null;
            
            vergadering.Actief = !vergadering.Actief;
            Db.Entry(vergadering).State = EntityState.Modified;
            Db.SaveChanges();
            return vergadering;
        }

        public Agendapunt ToggleActiveAgendapunt(int id)
        {
            Agendapunt agendapunt = Db.Agendapunt.SingleOrDefault(x => x.Id == id);
            if (agendapunt == null) return null;
            
            agendapunt.Actief = !agendapunt.Actief;
            Db.Entry(agendapunt).State = EntityState.Modified;
            Db.SaveChanges();
            return agendapunt;
        }

        public void AddNewDocsToDb(string fileName, int id)
        {
            Verslag verslag = new Verslag
            {
                Bestandsnaam = fileName,
                Afbeelding = false
            };
            Db.Verslag.Add(verslag);
            Db.SaveChanges();

            Vergadering vergadering = Db.Vergadering.SingleOrDefault(x => x.Id == id);

            if (vergadering != null)
            {
                vergadering.Verslag = verslag;
                vergadering.VerslagAanwezig = true;

                Db.Entry(vergadering).State = EntityState.Modified;
                Db.SaveChanges();
            }
            
        }

        public Verslag GetFileById(int id)
        {
            return (Db.Verslag.Find(id));
        }

        public void RemoveDocFromDb(int id)
        {
            Vergadering vergadering = Db.Vergadering.SingleOrDefault(x => x.Verslag.Id == id);
            if (vergadering != null)
            {
                Db.Verslag.Remove(vergadering.Verslag);
                Db.SaveChanges();

                vergadering.Verslag = null;
                vergadering.VerslagAanwezig = false;

                Db.Entry(vergadering).State = EntityState.Modified;
                Db.SaveChanges();
            }
            

                        
        }
    }
}
