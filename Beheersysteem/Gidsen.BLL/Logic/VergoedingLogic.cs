﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;

namespace Gidsen.BLL.Logic
{
    public class VergoedingLogic : BaseLogic
    {
        private readonly VergoedingListVm _vlVm;
        private readonly VergoedingVm _vVm;

        public VergoedingLogic()
        {
            _vlVm = new VergoedingListVm();
            _vVm = new VergoedingVm();
        }

        public VergoedingListVm GetAllVergoedingen()
        {
            List<Vergoeding> vergoedingList = Db.Vergoeding.ToList();
            if (vergoedingList.Any())
            {
                _vlVm.LijstVergoedingen = vergoedingList;
            }
            else
            {
                _vlVm.AddMessage("Er werden nog geen vergoedingen bepaald.", Message.MessageSeverity.Error);
            }

            return _vlVm;
        }

        public VergoedingVm GetVergoedingById(int id)
        {
            Vergoeding vergoeding = Db.Vergoeding.FirstOrDefault(x => x.Id == id);
            if (vergoeding != null)
            {
                _vVm.Id = vergoeding.Id;
                _vVm.Jaar = vergoeding.Jaar;
                _vVm.Hoofdbedrag = vergoeding.Hoofdbedrag;
                _vVm.Steunproject = vergoeding.Steunproject;
            }
            else
            {
                _vVm.AddMessage("Er ging iets mis. De gevraagde vergoeding werd niet teruggevonden", Message.MessageSeverity.Error);
            }

            return _vVm;
        }

        public void AddOrUpdateVergoeding(VergoedingVm model)
        {
            Vergoeding vergoeding = new Vergoeding
            {
                Id = model.Id,
                Jaar = model.Jaar,
                Hoofdbedrag = model.Hoofdbedrag,
                Steunproject = model.Steunproject
            };

            Db.Entry(vergoeding).State = vergoeding.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public Vergoeding ToggleActive(int id)
        {
            Vergoeding vergoeding = Db.Vergoeding.FirstOrDefault(x => x.Id == id);
            if (vergoeding == null) return null;

            vergoeding.Actief = !vergoeding.Actief;
            Db.Entry(vergoeding).State = EntityState.Modified;
            Db.SaveChanges();

            return vergoeding;
        }
    }
}
