﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gidsen.BOL.Models;
using System.Data.Entity;
using System.Security.Principal;
using Gidsen.BLL.Tools;
using Microsoft.AspNet.Identity;
using Gidsen.BOL.ViewModels;
using System.Web;

namespace Gidsen.BLL.Logic
{
    public class AgendaLogic: BaseLogic
    {
        private readonly GidsbeurtListVm _glVm;
        private readonly MailTool _tool;

        public AgendaLogic()
        {
            _glVm = new GidsbeurtListVm();
            _tool = new MailTool();
        }

       public List<CalendarData> GetCalendarData(DateTime fromDate, DateTime toDate)
        {
            var qry =
                from g in Db.Gidsbeurt
                where g.Goedgekeurd && g.Actief && g.StartDate >= fromDate && g.StartDate <= toDate
                select g;

            List<Gidsbeurt> lijst = qry.ToList();

            return (from item in lijst
                let typeTekst = item.GidsToegewezen ? "toegewezen" : "niet_toegewezen"
                select new CalendarData
                {
                    Id = item.Id,
                    StartDate = item.StartDate.ToString("yyyy-MM-dd H:mm:ss"),
                    EndDate = item.EndDate.ToString("yyyy-MM-dd H:mm:ss"),
                    Text = item.Text,
                    Type = typeTekst
                }).ToList();
        }

        public Gidsbeurt GetGidsbeurtById(int id)
        {
            return Db.Gidsbeurt
                .Include(x => x.User)
                .Include(x => x.GidsbeurtUitvoerder.Select(s => s.GidsRol))
                .SingleOrDefault(x => x.Id == id);
        }

        public GidsbeurtListVm GetAlleOpenstaande()
        {
            List<Gidsbeurt> gidsbeurtLijst = Db.Gidsbeurt
                .Where(x => x.GidsToegewezen == false && x.Goedgekeurd && x.Actief && x.StartDate > DateTime.Now)
                .OrderBy(x => x.StartDate)
                .ToList();

            if (gidsbeurtLijst.Any())
            {
                _glVm.GidsbeurtLijst = gidsbeurtLijst;
            }
            else
            {
                _glVm.AddMessage("Er zijn geen openstaande gidsbeurten terug te vinden", Message.MessageSeverity.Error);
            }

            return _glVm;
        }
        
        public bool GidsToevoegen(int id, string gidsRol, string userId, IPrincipal user)
        {
            if (string.IsNullOrEmpty(userId))
            {
                userId = user.Identity.GetUserId();
            }

            string rolId = Db.Roles.Where(x => x.Name.Equals("Stagiair")).Select(x => x.Id).FirstOrDefault();
            ApplicationUser gebruiker = Db.Users.Include(x => x.Roles).FirstOrDefault(x => x.Id.Equals(userId));

            var identityUserRole = gebruiker?.Roles.FirstOrDefault();
            if (identityUserRole != null && identityUserRole.RoleId.Equals(rolId) && gidsRol.Equals("Uitvoerder"))
            {
                var recordCount = Db.GidsbeurtUitvoerder.Count(x => x.User.Id.Equals(gebruiker.Id));
                if (recordCount < 4) //Nog niet genoeg gidsbeurten gevolgd, om er zelf één te mogen leiden
                {
                    return false;
                }
            }

            Gidsbeurt gidsbeurt = Db.Gidsbeurt.Find(id);
            if (gidsRol.Equals("Uitvoerder"))
            {
                if (gidsbeurt != null)
                {
                    gidsbeurt.GidsToegewezen = true;
                    Db.Entry(gidsbeurt).State = EntityState.Modified;
                    Db.SaveChanges();
                }             
            }

            GidsbeurtUitvoerder gbUitvoerder = new GidsbeurtUitvoerder
            {
                Gidsbeurt = gidsbeurt,
                GidsRol = Db.GidsRol.SingleOrDefault(x => x.Benaming.Equals(gidsRol)),
                User = gebruiker
            };
            Db.GidsbeurtUitvoerder.Add(gbUitvoerder);
            Db.SaveChanges();
            return true;
        }

        public void GidsVerwijderen(int gidsbeurtId, string userId, HttpServerUtilityBase server)
        {
            GidsbeurtUitvoerder gbUitvoerder = Db.GidsbeurtUitvoerder.Include(x => x.GidsRol).FirstOrDefault(x => x.Gidsbeurt.Id == gidsbeurtId && x.User.Id.Equals(userId));

            if (gbUitvoerder != null)
            {
                if (gbUitvoerder.GidsRol.Benaming.Equals("Uitvoerder"))
                {
                    Gidsbeurt gidsbeurt = Db.Gidsbeurt.Find(gidsbeurtId);
                    if (gidsbeurt != null)
                    {
                        gidsbeurt.GidsToegewezen = false;
                        Db.Entry(gidsbeurt).State = EntityState.Modified;
                        Db.SaveChanges();

                        var roleIds = Db.Roles.Where(x => x.Name.Equals("Stagiair") || x.Name.Equals("Gids") || x.Name.Equals("Beheerder")).Select(x => x.Id)
                            .ToList();

                        var gidsenLijst = Db.Users.Where(r => roleIds.Contains(r.Roles.FirstOrDefault().RoleId) && r.Actief)
                            .OrderBy(x => x.Familienaam).ThenBy(x => x.Voornaam).ToList();

                        if (gidsenLijst.Any())
                            foreach (var gids in gidsenLijst)
                                _tool.GmailMessage(gids, "", MailTool.EmailSoort.VrijgevenGidsbeurt, gidsbeurt.StartDate,
                                    server);
                    }
                }
                Db.GidsbeurtUitvoerder.Remove(gbUitvoerder);
                Db.SaveChanges();
            }
            
        }

        public List<ApplicationUser> GetGidsenLijst()
        {
            List<string> rolIds = Db.Roles.Where(x => x.Name.Equals("Gids") || x.Name.Equals("Stagiair") || x.Name.Equals("Beheerder")).Select(x => x.Id).ToList();
            var query =
                from gebruiker in Db.Users
                where rolIds.Contains(gebruiker.Roles.FirstOrDefault().RoleId) && gebruiker.Actief
                orderby gebruiker.Familienaam, gebruiker.Voornaam
                select gebruiker;

            List<ApplicationUser> gebruikersLijst = query.ToList();

            if (gebruikersLijst.Any())
                return gebruikersLijst;
            return null;
            
        }

        public List<ApplicationUser> ResetAndReturnList(int gidsbeurtId, string userId, HttpServerUtilityBase server)
        {
            GidsVerwijderen(gidsbeurtId, userId, server);
            
            return GetGidsenLijst();           
        }

        
    }
}
