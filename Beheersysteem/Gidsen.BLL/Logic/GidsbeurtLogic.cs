﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Gidsen.BLL.Tools;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using Microsoft.AspNet.Identity;

namespace Gidsen.BLL.Logic
{
    public class GidsbeurtLogic : BaseLogic
    {
        private readonly MailTool _tool;
        private GidsbeurtVm _gbVm;
        private readonly GidsbeurtListVm _gblVm;

        public GidsbeurtLogic()
        {
            _gbVm = new GidsbeurtVm();
            _gblVm = new GidsbeurtListVm();
            _tool = new MailTool();
        }

        public List<ApplicationUser> GetKlantenLijst()
        {
            var rolId = Db.Roles.Where(x => x.Name.Equals("Klant")).Select(x => x.Id).FirstOrDefault();
            var query =
                from gebruiker in Db.Users
                where gebruiker.Roles.FirstOrDefault().RoleId.Equals(rolId)
                orderby gebruiker.Familienaam, gebruiker.Voornaam
                select gebruiker;

            var gebruikersLijst = query.ToList();

            if (gebruikersLijst.Any())
                return gebruikersLijst;
            return null;
        }

        public GidsbeurtListVm GetAllGoedTeKeurenGidsbeurten()
        {
            var gidsbeurtList = Db.Gidsbeurt
                .Include(x => x.GidsbeurtUitvoerder)
                .Include(x => x.GidsbeurtUitvoerder.Select(c => c.GidsRol))
                .Include(x => x.GidsbeurtUitvoerder.Select(d => d.User))
                .Where(r => r.Goedgekeurd == false && r.Actief)
                .OrderBy(x => x.StartDate)
                .ToList();

            if (gidsbeurtList.Any())
                _gblVm.GidsbeurtLijst = gidsbeurtList;
            else
                _gblVm.AddMessage("Er zijn geen goed te keuren gidsbeurten", Message.MessageSeverity.Error);

            return _gblVm;
        }

        public GidsbeurtListVm GetAllGidsbeurtenPerJaar(int? jaartal)
        {
            if (jaartal == null)
            {
                _gblVm.AddMessage("U moet een geldig jaartal kiezen!", Message.MessageSeverity.Error);
            }
            else
            {
                var gidsbeurtList = Db.Gidsbeurt
                    .Include(x => x.GidsbeurtUitvoerder)
                    .Include(x => x.GidsbeurtUitvoerder.Select(c => c.GidsRol))
                    .Include(x => x.GidsbeurtUitvoerder.Select(d => d.User))
                    .Where(r => r.StartDate.Year == jaartal && r.Actief)
                    .OrderBy(x => x.StartDate)
                    .ToList();

                if (gidsbeurtList.Any())
                    _gblVm.GidsbeurtLijst = gidsbeurtList;
                else
                    _gblVm.AddMessage("Er werden geen gidsbeurten gevonden voor uw selectie",
                        Message.MessageSeverity.Error);
            }

            return _gblVm;
        }

        public GidsbeurtListVm GetAllGidsbeurtenPerGids(IPrincipal user, int? jaartal)
        {
            if (jaartal == null)
            {
                _gblVm.AddMessage("U moet een geldig jaartal kiezen!", Message.MessageSeverity.Error);
            }
            else
            {
                var userId = user.Identity.GetUserId();

                var gidsbeurtUitvoerdersList = Db.GidsbeurtUitvoerder.Include(x => x.GidsRol).Include(x => x.Gidsbeurt)
                    .Where(x => x.User.Id.Equals(userId) && x.Gidsbeurt.StartDate.Year == jaartal &&
                                x.Gidsbeurt.Actief).ToList();

                var gidsbeurten = gidsbeurtUitvoerdersList.Select(x => x.Gidsbeurt).Distinct();
                var gidsbeurtList = gidsbeurten.OrderBy(x => x.StartDate).ToList();

                if (gidsbeurtList.Any())
                {
                    _gblVm.GidsbeurtLijst = gidsbeurtList;

                    var vergoeding = Db.Vergoeding.Where(x => x.Jaar <= jaartal && x.Actief)
                        .OrderByDescending(x => x.Jaar).Take(1).FirstOrDefault();

                    if (vergoeding != null)
                        _gblVm.Vergoeding = vergoeding;

                    var steunProject = Db.SteunProject
                        .Where(x => x.Jaartal <= jaartal && x.User.Id.Equals(userId) && x.Actief)
                        .OrderByDescending(x => x.Jaartal).Take(1).FirstOrDefault();

                    if (steunProject != null)
                        _gblVm.Steunproject = true;
                }
                else
                {
                    _gblVm.AddMessage("Er werden geen gekoppelde gidsbeurten gevonden voor uw selectie",
                        Message.MessageSeverity.Error);
                }
            }

            return _gblVm;
        }

        public GidsbeurtListVm GetAllGidsbeurtenOfKlant(IPrincipal user)
        {
            var userId = user.Identity.GetUserId();


            var qry = from gidsbeurt in Db.Gidsbeurt
                    .Include(x => x.GidsbeurtUitvoerder)
                    .Include(x => x.GidsbeurtUitvoerder.Select(c => c.GidsRol))
                    .Include(x => x.GidsbeurtUitvoerder.Select(d => d.User))
                    .Where(x => x.User.Id.Equals(userId) && x.Actief).OrderBy(x => x.StartDate)
                select gidsbeurt;

            var gidsbeurtList = qry.ToList();

            if (gidsbeurtList.Any())
                _gblVm.GidsbeurtLijst = gidsbeurtList;
            else
                _gblVm.AddMessage("Er werden geen gidsbeurten teruggevonden!", Message.MessageSeverity.Warning);
            return _gblVm;
        }


        public GidsbeurtVm GetFormData(IPrincipal user)
        {
            _gbVm.ListLeeftijden = Db.LeeftijdCategorie.Where(x => x.Actief).OrderBy(x => x.Beschrijving).ToList();
            _gbVm.ThemaLijst = Db.Thema.Where(x => x.Actief).OrderBy(x => x.Naam).ToList();
            if (user.IsInRole("Admin") || user.IsInRole("Beheerder"))
                _gbVm.KlantenLijst = GetKlantenLijst();

            _gbVm.Datum = DateTime.Today.AddDays(14);

            _gbVm.AddMessage(
                "<strong>Opgelet!</strong>&nbsp;Om organisatorische redenen, dient u een gidsbeurt minstens twee weken op voorhand aan te vragen. ",
                Message.MessageSeverity.Warning);

            _gbVm.AddMessage(
                "<strong>Opgelet!</strong>&nbsp;voor een gidsbeurt, moet u twee uur rekenen. Hou hier rekening mee bij de aanvraag van optie's (eten/show)",
                Message.MessageSeverity.Warning);

            return _gbVm;
        }

        public GidsbeurtVm GetFormDataById(int id, IPrincipal user)
        {
            _gbVm = GetFormData(user);

            var gidsbeurt = Db.Gidsbeurt
                .Include(x => x.Thema)
                .Include(x => x.LeeftijdCategorie)
                .Include(x => x.User)
                .SingleOrDefault(x => x.Id == id);

            if (gidsbeurt != null)
            {
                _gbVm.Id = gidsbeurt.Id;
                _gbVm.Datum = gidsbeurt.StartDate;
                _gbVm.Text = gidsbeurt.Text;
                _gbVm.Deelnemers = gidsbeurt.Deelnemers;
                _gbVm.LeeftijdCategorieId = gidsbeurt.LeeftijdCategorie.Id;
                _gbVm.ThemaId = gidsbeurt.Thema.Id;
                _gbVm.Rolstoel = gidsbeurt.Rolstoel;
                _gbVm.ShowNaGidsbeurt = gidsbeurt.ShowNaGidsbeurt;
                _gbVm.EtenNaGidsbeurt = gidsbeurt.EtenNaGidsbeurt;
                _gbVm.Inlichtingen = gidsbeurt.Inlichtingen;
                _gbVm.UserId = gidsbeurt.User.Id;
                _gbVm.GidsToegewezen = gidsbeurt.GidsToegewezen;
                _gbVm.Goedgekeurd = gidsbeurt.Goedgekeurd;
            }

            if (_gbVm.Goedgekeurd)
                _gbVm.AddMessage(
                    "Uw aanvraag werd reeds goedgekeurd op basis van uw opgegeven voorkeuren. Enkele velden kan u niet meer wijzigen.",
                    Message.MessageSeverity.Warning);

            return _gbVm;
        }

        public Gidsbeurt Bepaling(GidsbeurtVm model, IPrincipal user)
        {
            var userId = string.IsNullOrEmpty(model.UserId) ? user.Identity.GetUserId() : model.UserId;

            var gebruiker = Db.Users.Find(userId);
            Gidsbeurt gidsbeurt;

            if (model.Id == 0)
            {
                bool goedgekeurd = user.IsInRole("Beheerder") || user.IsInRole("Admin");
                gidsbeurt = new Gidsbeurt
                {
                    StartDate = model.Datum,
                    EndDate = model.Datum.AddHours(2),
                    Text = model.Text,
                    Deelnemers = model.Deelnemers,
                    LeeftijdCategorie = Db.LeeftijdCategorie.Find(model.LeeftijdCategorieId),
                    Thema = Db.Thema.Find(model.ThemaId),
                    Rolstoel = model.Rolstoel,
                    EtenNaGidsbeurt = model.EtenNaGidsbeurt,
                    ShowNaGidsbeurt = model.ShowNaGidsbeurt,
                    Inlichtingen = model.Inlichtingen,
                    User = gebruiker,
                    Goedgekeurd = goedgekeurd
                };
            }
            else
            {
                gidsbeurt = Db.Gidsbeurt.SingleOrDefault(x => x.Id == model.Id);
                if (gidsbeurt == null) return null;
                gidsbeurt.StartDate = model.Datum;
                gidsbeurt.EndDate = model.Datum.AddHours(2);
                gidsbeurt.Text = model.Text;
                gidsbeurt.Deelnemers = model.Deelnemers;
                gidsbeurt.LeeftijdCategorie = Db.LeeftijdCategorie.Find(model.LeeftijdCategorieId);
                gidsbeurt.Thema = Db.Thema.Find(model.ThemaId);
                gidsbeurt.Rolstoel = model.Rolstoel;
                gidsbeurt.EtenNaGidsbeurt = model.EtenNaGidsbeurt;
                gidsbeurt.ShowNaGidsbeurt = model.ShowNaGidsbeurt;
                gidsbeurt.Inlichtingen = model.Inlichtingen;
                gidsbeurt.User = gebruiker;
            }

            return gidsbeurt;
        }


        /*public void AddGidsbeurtToDb(GidsbeurtVm model, IPrincipal user)
        {
            var gidsbeurt = Bepaling(model, user);
            Db.Gidsbeurt.Add(gidsbeurt);
            Db.SaveChanges();
        }*/

        public void AddOrUpdateGidsbeurt(GidsbeurtVm model, IPrincipal user, HttpServerUtilityBase server)
        {
            var gidsbeurt = Bepaling(model, user);

            if (gidsbeurt.Id == 0)
            {
                if (user.IsInRole("Klant"))
                {
                    VerwittigBeheerders(gidsbeurt, server);
                    
                }
                else
                {
                    VerwittigGidsen(gidsbeurt, server);
                }
            }
            
            Db.Entry(gidsbeurt).State = gidsbeurt.Id == 0 ? EntityState.Added : EntityState.Modified;

            Db.SaveChanges();
        }
        
        public Gidsbeurt ToggleActive(int id, HttpServerUtilityBase server)
        {
            var gidsbeurt = Db.Gidsbeurt.Include(x => x.GidsbeurtUitvoerder.Select(d => d.User))
                .SingleOrDefault(x => x.Id == id);
            if (gidsbeurt != null)
            {
                gidsbeurt.Actief = !gidsbeurt.Actief;
                Db.Entry(gidsbeurt).State = EntityState.Modified;
                Db.SaveChanges();

                if (!gidsbeurt.GidsToegewezen || gidsbeurt.Actief) return gidsbeurt;
                var uitvoerders = gidsbeurt.GidsbeurtUitvoerder.ToList();
                foreach (var gids in uitvoerders)
                    _tool.GmailMessage(gids.User, "", MailTool.EmailSoort.AnnulatieGidsbeurt, gidsbeurt.StartDate,
                        server);
            }
            return gidsbeurt;
        }

        public void ToggleGoedgekeurd(string status, int gidsbeurtId, HttpServerUtilityBase server)
        {
            var gidsbeurt = Db.Gidsbeurt.Include(x => x.User).SingleOrDefault(x => x.Id == gidsbeurtId);

            if (status.Equals("goedkeuren"))
            {
                if (gidsbeurt != null)
                {
                    gidsbeurt.Goedgekeurd = true;

                    VerwittigGidsen(gidsbeurt, server);
                }
            }
            else
            {
                if (gidsbeurt != null)
                {
                    gidsbeurt.Goedgekeurd = false;
                    gidsbeurt.Actief = false;

                    _tool.GmailMessage(gidsbeurt.User, "", MailTool.EmailSoort.AfkeuringGidsbeurt, gidsbeurt.StartDate,
                        server);
                }
            }
            Db.Entry(gidsbeurt).State = EntityState.Modified;
            Db.SaveChanges();
        }

        private void VerwittigGidsen(Gidsbeurt gidsbeurt, HttpServerUtilityBase server)
        {
            var roleIds = Db.Roles.Where(x => x.Name.Equals("Stagiair") || x.Name.Equals("Gids")).Select(x => x.Id)
                .ToList();

            var gidsenLijst = Db.Users.Where(r => roleIds.Contains(r.Roles.FirstOrDefault().RoleId) && r.Actief)
                .OrderBy(x => x.Familienaam).ThenBy(x => x.Voornaam).ToList();

            if (gidsenLijst.Any())
                foreach (var gids in gidsenLijst)
                    _tool.GmailMessage(gids, "", MailTool.EmailSoort.GoedkeuringGidsbeurt, gidsbeurt.StartDate,
                        server);
        }

        private void VerwittigBeheerders(Gidsbeurt gidsbeurt, HttpServerUtilityBase server)
        {
            var rolId = Db.Roles.Where(x => x.Name.Equals("Beheerder")).Select(x => x.Id).FirstOrDefault();
            var beheerdersLijst = Db.Users
                .Include(x => x.Roles)
                .Where(x => x.Roles.FirstOrDefault().RoleId.Equals(rolId)).ToList();

            foreach (var beheerder in beheerdersLijst)
                _tool.GmailMessage(beheerder, "", MailTool.EmailSoort.AanvraagGidsbeurt, gidsbeurt.StartDate,
                    server);
        }
    }
}