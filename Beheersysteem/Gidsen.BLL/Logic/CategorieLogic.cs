﻿using System.Collections.Generic;
using System.Linq;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Models;
using System.Data.Entity;

namespace Gidsen.BLL.Logic
{
    public class CategorieLogic : BaseLogic
    {
        private readonly CategorieListVm _clVm;
        private readonly CategorieVm _cVm;

        public CategorieLogic()
        {
            _clVm = new CategorieListVm();
            _cVm = new CategorieVm();
        }
        public CategorieListVm GetAllCategorie()
        {
            List<Categorie> categorieLijst = Db.Categorie.ToList();
            if (categorieLijst.Any())
            {
                _clVm.CategorieLijst = categorieLijst;
            }
            else
            {
                _clVm.AddMessage("Er werden nog geen categorieën gevonden", Message.MessageSeverity.Error);
            }
            return _clVm;
        }

        public CategorieVm GetCategorieById(int id)
        {
            Categorie categorie = Db.Categorie.FirstOrDefault(x => x.Id == id);
            if (categorie != null)
            {
                _cVm.Id = categorie.Id;
                _cVm.Naam = categorie.Naam;
            }
            else
            {
                _cVm.AddMessage("Er ging iets mis. De gevraagde categorie werd niet teruggevonden", Message.MessageSeverity.Error);
            }
            return _cVm;
        }

        public void AddOrUpdateCategorie(CategorieVm model)
        {
            Categorie categorie = new Categorie
            {
                Id = model.Id,
                Naam = model.Naam
            };

            Db.Entry(categorie).State = categorie.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public Categorie ToggleActive(int id)
        {
            Categorie categorie = Db.Categorie.SingleOrDefault(x => x.Id == id);
            if (categorie == null) return null;
            categorie.Actief = !categorie.Actief;
            Db.Entry(categorie).State = EntityState.Modified;
            Db.SaveChanges();
            return categorie;

        }
    }
}
