﻿$.fn.reload = function () {
    $(".overzicht").html(spinner);
    $.ajax({
        url: controller + 'List',
        async: true,
        type: "GET",
        success: function (resp) {
            $(".overzicht").html(resp);
            $.fn.removeMessage();
            $.fn.tooltips();
        }
    });
}

$.fn.reload();