﻿ <!DOCTYPE html>
  <html>
 <head>  
    <meta charset="utf-8" />
    <title>Annulatie gidsbeurt</title>
</head>  

   <body>  
    	  
    <div style='width:600px; height:350px; padding-left:30px; padding-right:30px; margin-top:10px;'>

    <p style="font-family: Arial;font-size:16px;">Beste <strong>{0}</strong>,<p>

    <p style="font-family: Arial;font-size:16px;">Wij kregen van de klant de vraag om een gidsbeurt te schrappen, waarvoor u voorzien was als gids (of als volger).</p> 
	<p style="font-family: Arial;font-size:16px;">U ontvangt deze mail om u te verwittigen zodat u niet onnodig komt opdagen voor deze gidsbeurt</p> 
    <p style="font-family: Arial;font-size:16px;"><u>De gidsbeurt in kwestie was voorzien op</u>:</p>
    <p style="font-family: Arial;font-size:16px;">{1}</p>
	
    <p style="font-family: Arial;font-size:16px;">Vriendelijke groeten, <br><br><br>
	<em>De Olmense Zoo</em>
	</p>
	
    </div>
  </body>
 </html>