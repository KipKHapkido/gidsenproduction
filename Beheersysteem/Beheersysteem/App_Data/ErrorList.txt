﻿var errors = ModelState
    .Where(x => x.Value.Errors.Count > 0)
    .Select(x => new { x.Key, x.Value.Errors })
    .ToArray();

IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);