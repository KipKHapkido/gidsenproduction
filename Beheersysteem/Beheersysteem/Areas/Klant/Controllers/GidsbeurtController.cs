﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Interfaces;

namespace Beheersysteem.Areas.Klant.Controllers
{
    public class GidsbeurtController : Controller, IList
    {
        private readonly GidsbeurtLogic _logic = new GidsbeurtLogic();

        // GET: Klant/Gidsbeurt/UserOverView
        [Authorize]
        public PartialViewResult List(int? jaartal)
        {
            GidsbeurtListVm gblVm; 
            if (User.IsInRole("Klant"))
            {
                gblVm = _logic.GetAllGidsbeurtenOfKlant(User);
            }
            else if (User.IsInRole("Gids") || User.IsInRole("Stagiair"))
            {
                ViewBag.Title = "";
                gblVm = _logic.GetAllGidsbeurtenPerGids(User, jaartal);
            } 
            else
            {
                gblVm = _logic.GetAllGoedTeKeurenGidsbeurten();
            }
            
            return PartialView("_gidsbeurtList", gblVm);
        }


        // GET: Klant/Gidsbeurt/
        [Authorize(Roles = "Klant")]
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Title = "Aanvraag";
            ViewBag.Soort = "new";
            return View("CreateUpdate", _logic.GetFormData(User));
        }

        [Authorize(Roles = "Klant")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(GidsbeurtVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateGidsbeurt(model, User, Server);

                return RedirectToAction("UserDetails", "Account", new { area = "Algemeen" });
            }

            GidsbeurtVm gcVm;
            
            if (model.Id > 0)
            {
                ViewBag.Title = "Wijzig";
                ViewBag.Soort = "edit";
                gcVm = _logic.GetFormDataById(model.Id, User);
            }
            else
            {
                ViewBag.Title = "Aanvraag";
                ViewBag.Soort = "new";
                gcVm = _logic.GetFormData(User);
            }
            model.ListMessage = gcVm.ListMessage;
            model.ListLeeftijden = gcVm.ListLeeftijden;
            model.ThemaLijst = gcVm.ThemaLijst;
            model.GidsToegewezen = gcVm.GidsToegewezen;

            return View("CreateUpdate", model);
        }

        // GET: Klant/Gidsbeurt/Edit/5
        [Authorize(Roles = "Klant")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            GidsbeurtVm gcVm = _logic.GetFormDataById(id, User);
            if (gcVm == null)
            {
                return HttpNotFound();
            }

            ViewBag.Title = "Wijzig";
            ViewBag.Soort = "edit";
            return View("CreateUpdate", gcVm);
        }

        [Authorize(Roles = "Klant")]
        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gidsbeurt gidsbeurt = _logic.ToggleActive(id, Server);

            return Json(gidsbeurt != null);
        }

        [Authorize(Roles = "Admin,Beheerder")]
        [HttpPost]
        public ActionResult BepaalStatus(string status, int gidsbeurtId)
        {
            _logic.ToggleGoedgekeurd(status, gidsbeurtId, Server);
            return Json(true);
        }
    }
}
