﻿using System;
using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Models;
using System.Net;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class VergoedingController : Controller, ICrudGeneric<int, VergoedingVm>, IList, IBreadCrumb
    {
        private readonly VergoedingLogic _logic = new VergoedingLogic();

        // GET: Beheerder/Vergoeding
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_vergoedingList", _logic.GetAllVergoedingen());
        }

        public PartialViewResult Create()
        {
            VergoedingVm v_VM = new VergoedingVm
            {
                Jaar = DateTime.Now.Year
            };
            return PartialView("_createUpdate", v_VM);
        }

        public PartialViewResult Edit(int Id)
        {
            return PartialView("_createUpdate", _logic.GetVergoedingById(Id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(VergoedingVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateVergoeding(model);
                return Json(true);
            }

            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vergoeding vergoeding = _logic.ToggleActive(id);

            return Json(vergoeding != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Vergoeding", Beschrijving = "Vergoeding"}
            };
            
            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}