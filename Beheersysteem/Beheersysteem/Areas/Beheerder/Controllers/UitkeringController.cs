﻿using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Interfaces;
using Gidsen.BOL.Models;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class UitkeringController : Controller, IBreadCrumb
    {
        private readonly UitkeringLogic _logic = new UitkeringLogic();

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Uitkering", Beschrijving = "Uitkering"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }

        // GET: Beheerder/Uitkering
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? jaar)
        {
            return PartialView("_uitkeringList", _logic.GetAlleUitkeringen(jaar));
        }

        public PartialViewResult ListStagiairs()
        {
            return PartialView("_stagiairList", _logic.GetAllStagiairs());
        }
    }
}