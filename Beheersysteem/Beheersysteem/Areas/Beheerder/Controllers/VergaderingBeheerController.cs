﻿using Gidsen.BOL.Interfaces;
using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder,Gids,Stagiair")]
    public class VergaderingBeheerController : Controller, IBreadCrumb
    {
        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/VergaderingBeheer", Beschrijving = "Vergadering-beheer"}

            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }

        // GET: Beheerder/VergaderingBeheer
        public ActionResult Index()
        {
            return View();
        }
    }
}