﻿using System.Collections.Generic;
using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Models;
using System.Net;
using Gidsen.BOL.Interfaces;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class GidsbeurtController : Controller, ICrudGeneric<int, GidsbeurtVm>, IList, IBreadCrumb
    {
        private readonly GidsbeurtLogic _logic = new GidsbeurtLogic();

        // GET: Beheerder/Gidsbeurt
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? jaar)
        {
            return PartialView("_gidsbeurtList", _logic.GetAllGidsbeurtenPerJaar(jaar));
        }

        public PartialViewResult Create()
        {
            ViewBag.Soort = "new";
            return PartialView("_createUpdateGidsbeurt", _logic.GetFormData(User));
        }

        public PartialViewResult Edit(int id)
        {
            ViewBag.Soort = "edit";
            return PartialView("_createUpdateGidsbeurt", _logic.GetFormDataById(id, User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(GidsbeurtVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateGidsbeurt(model, User, Server);
                return Json(true);
            }
            GidsbeurtVm gcVm;

            if (model.Id > 0)
            {
                ViewBag.Soort = "edit";
                gcVm = _logic.GetFormDataById(model.Id, User);
            }
            else
            {
                ViewBag.Soort = "new";
                gcVm = _logic.GetFormData(User);
            }
                        
            model.ListMessage = gcVm.ListMessage;
            model.ListLeeftijden = gcVm.ListLeeftijden;
            model.ThemaLijst = gcVm.ThemaLijst;
            model.GidsToegewezen = gcVm.GidsToegewezen;
            
            if (User.IsInRole("Admin") || User.IsInRole("Beheerder"))
            {
                model.KlantenLijst = gcVm.KlantenLijst;
            }

            return PartialView("_createUpdateGidsbeurt", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gidsbeurt gidsbeurt = _logic.ToggleActive(id, Server);

            return Json(gidsbeurt != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/Gidsbeurt", Beschrijving = "Gidsbeurt"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}