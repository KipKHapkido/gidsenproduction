﻿using System.Web.Mvc;

namespace Beheersysteem.Areas.Beheerder
{
    public class BeheerderAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Beheerder";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Beheerder_default",
                "Beheerder/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}