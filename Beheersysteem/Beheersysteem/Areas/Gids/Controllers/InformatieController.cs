﻿using System.Web.Mvc;

namespace Beheersysteem.Areas.Gids.Controllers
{
    [Authorize(Roles = "Gids, Stagiair")]
    public class InformatieController : Controller
    {
        // GET: Gids/Informatie/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}