﻿$(document).ready(function () {
    if (!$.cookie("AcceptCookie")) {
        $(".cookie").css("display", "block");
    }

    $.fn.removeMessage = function() {
        setTimeout(function() {
                $(".alert").slideUp("normal").promise().done(function() {
                    $(".alert").remove();
                });
            },
            6000);
    };

    $.fn.tooltips = function() {
        $('[data-toggle="tooltip"]').tooltip();
    };

    $.ajax({
        url: "/Algemeen/Home/MenuLijst",
        async: true,
        type: "GET",
        success: function (resp) {
            $("ul.ajaxresp").html(resp);
        }
    });

    $(document).on("click", ".accept", function () {
        $(".cookie").slideUp("slow").promise().done(function () {
            $(".cookie").remove();
        });
        $.cookie("AcceptCookie", true, { expires: 7, path: "/" });
    });

    $.fn.tooltips();

});
var spinner = "<div class='loading'></div>";