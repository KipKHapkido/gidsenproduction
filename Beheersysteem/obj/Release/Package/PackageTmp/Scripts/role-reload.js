﻿$.fn.reload = function () {
    $(".overzicht").html(spinner);
    var dataString = "rol=" + $("#rol option:selected").val();

    $.ajax({
        url: controller + "List",
        async: true,
        type: "GET",
        data: dataString,
        success: function (resp) {
            $(".overzicht").html(resp);
            $.fn.removeMessage();
            $.fn.tooltips();

        }
    });
}

$(document).on("change", "#rol", function () {
    $.fn.reload();
});