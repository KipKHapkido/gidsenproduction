﻿$(document).on("click", ".gidsrol", function () {
    var dataString = "id=" + $("#gidsbeurtID").val() + "&GidsRol=" + $(this).val();
    $("#myModal").modal("hide");
    $.ajax({
        url: "/Gids/Agenda/OpgevenAlsGids",
        async: true,
        data: dataString,
        type: "POST",

        success: function (resp) {
            if (resp === true) {
                bootbox.alert("U werd aan de gidsbeurt gekoppeld!", function () {
                    reload();
                });
            } else {
                bootbox.alert("Volgens ons database hebt u nog niet voldoende gidsbeurten gevolgd, om zelf een gidsbeurt te mogen leiden!");
            }
        }
    });
});

$(document).on("click", ".deleteUser", function () {
    var dataString = "gidsbeurtID=" + $(this).attr("data-gidsbeurt") + "&userId=" + $(this).attr("data-gids");

    $("#myModal").modal("hide");
    $.ajax({
        url: "/Gids/Agenda/VerwijderGids",
        async: true,
        data: dataString,
        type: "POST",

        success: function (resp) {
            if (resp === true) {
                bootbox.alert("U werd van de gidsbeurt gehaald!", function () {
                    reload();
                });

            } else {
                bootbox.alert("Er ging iets mis. We konden u niet verwijderen van deze gidsbeurt");
            }
        }
    });
});

$(document).on("click", ".resetUser", function () {
    var dataString = "gidsbeurtID=" + $(this).attr("data-gidsbeurt") + "&userId=" + $(this).attr("data-gids");
    var gidsRol = $(this).attr("data-gidsrol");

    $.ajax({
        url: "/Gids/Agenda/ResetUser",
        async: true,
        data: dataString,
        type: "POST",

        success: function (resp) {
            if (gidsRol === "Uitvoerder") {
                $("#uitvoerder").prop('disabled', false);
                $("#uitvoerder").html(resp);
            } else {
                $("#volger").prop('disabled', false);
                $("#volger").html(resp);
            }
        }
    });
});

$(document).on("change", "#uitvoerder, #volger", function () {
    $("#myModal").modal("hide");
    if ($(this).find("option:selected").val() !== "") {
        var dataString = "id=" + $(this).attr("data-gidsbeurt") + "&gidsRol=" + $(this).attr("data-gidsrol") + "&userId=" + $(this).find("option:selected").val();

        $.ajax({
            url: "/Gids/Agenda/OpgevenAlsGids",
            async: true,
            data: dataString,
            type: "POST",

            success: function (resp) {
                if (resp === true) {
                    bootbox.alert("De persoon werd aan de gidsbeurt gekoppeld!", function () {
                        reload();
                    });
                } else {
                    bootbox.alert("Volgens ons database heeft de gekozen persoon nog niet voldoende gidsbeurten gevolgd, om zelf een gidsbeurt te mogen leiden!");
                }
            }
        });
    }
});

$.fn.details = function (id) {
    if (id > 0) {
        $(".modal-title").html("Details gidsbeurt");
        $("#myModal").modal("show");
        $(".modal-body").html(spinner);

        var dataString = "id=" + id;
        $.ajax({
            url: "/Gids/Agenda/Details",
            async: true,
            data: dataString,
            type: "GET",
            success: function (resp) {               
                $(".modal-body").html(resp);                
            }
        });

    }
}

$(document).on("click", ".toggle-active2", function () {
    var dataString = "id=" + $(this).val();
    bootbox.confirm("Bent u zeker dat u deze gidsbeurt wil (de)activeren?", function (result) {
        if (result) {
            $.ajax({
                url: "/Klant/Gidsbeurt/ToggleActive",
                async: true,
                data: dataString,
                type: "POST",
                success: function () {
                    reload();
                }
            });
        }
    });
});

$(document).on("click", ".status", function () {
    var waarde = $(this).val();
    var dataString = "status=" + waarde + "&gidsbeurtID=" + $("#gidsbeurtID").val();
    $("#myModal").modal("hide");

    bootbox.confirm("Bent u zeker dat u deze gidsbeurt wil " + waarde + "?", function (result) {
        if (result) {
            $.ajax({
                url: "/Klant/Gidsbeurt/BepaalStatus",
                async: true,
                data: dataString,
                type: "POST",
                success: function () {
                    reload();
                }
            });
        }
    });
});

$(document).on("change", "#jaartal", function () {
    reload();
});

$(document).on("mouseover", ".deleteUser, .resetUser", function () {
    $(this).css("cursor", "pointer");
});