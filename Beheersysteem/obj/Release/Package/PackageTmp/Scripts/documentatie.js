﻿$(document).ready(function () {
    $.ajax({
        url: controller + 'BreadCrumbs',
        async: true,
        type: "GET",
        success: function (resp) {
            $("#breadcrumb").html(resp);
            $.fn.tooltips();
        }
    });

    $(document).on("click", '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $.fn.reload = function () {
        $(".overzicht").html("<div class='loading'></div>");
        
        var dataString = "id=" + $("#Id option:selected").val();
        $.ajax({
            url: controller + "Lijst",
            async: true,
            data: dataString,
            type: "GET",
            success: function (resp) {
                $(".overzicht").html(resp);
                $.fn.removeMessage();
            }
        });
    }

    $(document).on("change", "#Id", function () {
        $.fn.reload();
    });

    $(document).on("click", ".deleteFile", function () {
        var id = $(this).attr("data-id");

        var dataString = "id=" + id;
        bootbox.confirm("Bent u zeker dat u deze documentatie wil verwijderen?", function (result) {
            if (result) {
                $.ajax({
                    url: controller + "DeleteFile",
                    async: true,
                    data: dataString,
                    type: "POST",
                    success: function (resp) {
                        if (resp) {
                            $.fn.reload();
                        }
                    }
                });
            }
        });
    });

    $(function () {

        $("#file_upload").uploadifive({
            'auto': true,
            'fileObjName': "file",
            'onUpload': function () {
                $("#file_upload").data("uploadifive").settings.formData = {
                    'id': $("#Id option:selected").val(),
                }
            },
            'queueID': "queue",
            'uploadScript': controller + "Upload",
            'onUploadComplete': function () {
                $.fn.reload();
            }
        });
    });
});