﻿<!DOCTYPE html
 <html>
<head>  
    <meta charset="utf-8" />
    <title>Bevestiging registratie</title>
</head>  

   <body>  
    	  
    <div style='width:600px; height:350px; padding-left:30px; padding-right:30px; margin-top:10px;'>

    <p style="font-family: Arial;font-size:16px;">Beste <strong>{0}</strong>,<p>

    <p style="font-family: Arial;font-size:16px;">Bedankt voor uw registratie.</p> 
    <p style="font-family: Arial;font-size:16px;"><u>Klik op onderstaande link om uw account te bevestigen en verder te gaan</u>:</p>
    <p style="font-family: Arial;font-size:16px;">
		<a href='{1}' style='display: inline-block; text-align:center; vertical-align: middle; text-decoration:none; line-height: 1.42857143; padding: 8px 16px; font-size: 18px; color: #fff; background-color: #5cb85c; border-color: #4cae4c;border-radius: 4px;'>Bevestig account</a>
	</p>
	
	  <p>&nbsp;</p> 
    <p style="font-family: Arial;font-size:16px;">Vriendelijke groeten, <br><br><br>
	<em>De Olmense Zoo</em>
	</p>
	<p>P.S.: Bent u gids en heeft u zichzelf geregistreerd? Geef dan nog een seintje naar de Olmense Zoo toe, zodat extra rechten kunnen toegekend worden.</p>
	
    </div>
  </body>
 </html>