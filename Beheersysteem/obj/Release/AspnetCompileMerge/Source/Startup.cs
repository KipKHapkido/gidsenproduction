﻿using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Gidsen.BOL.Models;
using Gidsen.DAL;
using System.Linq;

[assembly: OwinStartupAttribute(typeof(Beheersysteem.Startup))]
namespace Beheersysteem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
            CreateGidsRollen();
        }

        private void CreateGidsRollen()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            GidsRol rol = db.GidsRol.FirstOrDefault(x => x.Benaming.Equals("Uitvoerder"));
            if (rol == null)
            {
                GidsRol nieuweRol = new GidsRol
                {
                    Benaming = "Uitvoerder",
                    Actief = true
                };
                db.GidsRol.Add(nieuweRol);
                db.SaveChanges();
            }
            rol = db.GidsRol.FirstOrDefault(x => x.Benaming.Equals("Volger"));
            if (rol == null)
            {
                GidsRol nieuweRol = new GidsRol
                {
                    Benaming = "Volger",
                    Actief = true
                };
                db.GidsRol.Add(nieuweRol);
                db.SaveChanges();
            }
        }

        private void CreateRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User    
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool   
                var role = new IdentityRole()
                {
                    Name = "Admin"
                };
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                  

                var user = new ApplicationUser()
                {
                    UserName = "KipKHapkido",
                    Email = "sven.vervloet@gmail.com"
                };
                string userPwd = "Sveca413!";

                var chkUser = userManager.Create(user, userPwd);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Admin");
                }
            }

            // Aanmaken beheerder rol
            if (!roleManager.RoleExists("Beheerder"))
            {
                var role = new IdentityRole()
                {
                    Name = "Beheerder"
                };
                roleManager.Create(role);

                var user = new ApplicationUser()
                {
                    UserName = "Robby",
                    Email = "robbyv@olmensezoo.be"
                };
                string userPwd = "Bio@Zoo1";

                var chkUser = userManager.Create(user, userPwd);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Beheerder");
                }

                user = new ApplicationUser()
                {
                    UserName = "Els",
                    Email = "Els@olmensezoo.be"
                };
                userPwd = "Pers@Zoo1";

                chkUser = userManager.Create(user, userPwd);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Beheerder");

                }

            }

            // Aanmaken gids rol
            if (!roleManager.RoleExists("Gids"))
            {
                var role = new IdentityRole()
                {
                    Name = "Gids"
                };
                roleManager.Create(role);

                var user = new ApplicationUser()
                {
                    UserName = "Caroline",
                    Email = "caroline.vercruysse@gmail.com"
                };
                string userPwd = "Sveca413!";

                var chkUser = userManager.Create(user, userPwd);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Gids");

                }
            }

            if (!roleManager.RoleExists("Stagiair"))
            {
                var role = new IdentityRole()
                {
                    Name = "Stagiair"
                };
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Klant"))
            {
                var role = new IdentityRole()
                {
                    Name = "Klant"
                };
                roleManager.Create(role);

            }
        }
    }
}
