﻿using System.Web.Mvc;

namespace Beheersysteem.Areas.Algemeen
{
    public class AlgemeenAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Algemeen";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Algemeen_default",
                "Algemeen/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}