﻿using Gidsen.BLL.Logic;
using Gidsen.BLL.Tools;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Web;
using System.Web.Mvc;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder,Gids,Stagiair")]
    public class DierDocumentatieController : Controller, IBreadCrumb
    {
        private readonly DierDocumentatieLogic _ddLogic = new DierDocumentatieLogic();
        private readonly DocTool _doc = new DocTool();

        // GET: Beheerder/ThemaDocumentatie
        public ActionResult Overzicht()
        {
            return View(_ddLogic.GetAllActiveAnimals());
        }
               
        public PartialViewResult Lijst(int id)
        {
            return PartialView("_docLijst", _ddLogic.GetDocListByDierId(id));
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, int id)
        {
            string fileName = _doc.BestandUploaden(file, Server);

            if (!string.IsNullOrEmpty(fileName))
            {
                _ddLogic.AddNewDocsToDb(fileName, id);
                return Json(true);
            }
            return Json(false);
        }

        [HttpPost]
        public ActionResult DeleteFile(int id)
        {
            DierDocumentatie doc = _ddLogic.GetFileById(id);
            string bestandsnaam = doc.Bestandsnaam;

            bool gelukt = _doc.BestandVerwijderen(bestandsnaam, Server);
            if (gelukt)
            {
                _ddLogic.RemoveDocFromDb(id);
                return Json(true);
            }
            return Json(false);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>();
            if (User.IsInRole("Admin") || User.IsInRole("Beheerder"))
            {
                breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/DierenBeheer", Beschrijving = "Beheer dieren" });
            }
            breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/DierDocumentatie/Overzicht", Beschrijving = "Documentatie dieren" });
            
            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}