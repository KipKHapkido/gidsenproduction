﻿using System.Net;
using System.Web.Mvc;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Interfaces;
using System.Collections.Generic;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class CategoriesController : Controller, ICrudGeneric<int, CategorieVm>, IList, IBreadCrumb
    {
        private readonly CategorieLogic _logic = new CategorieLogic();

        // GET: Beheerder/Categories
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? keuze)
        {
            return PartialView("_categorieList", _logic.GetAllCategorie());
        }


        // GET: Beheerder/Categories/Create
        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", new CategorieVm());
        }

        // GET: Beheerder/Categories/Edit/5
        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetCategorieById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(CategorieVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateCategorie(model);
                return Json(true);
            }

            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categorie categorie = _logic.ToggleActive(id);

            if (categorie == null)
            {
                return Json(false);
            }
            return Json(true);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>
            {
                new BreadCrumb{ Url = "/Beheerder/DierenBeheer", Beschrijving = "Beheer dieren"},
                new BreadCrumb{ Url = "/Beheerder/Categories", Beschrijving = "Categorieën"}
            };

            return PartialView("_breadCrumbs", breadCrumbList);
        }
    }
}
