﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;
using System.Net;
using Gidsen.BOL.Interfaces;
using Gidsen.BLL.Tools;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder,Gids,Stagiair")]
    public class VergaderingController : Controller, ICrudGeneric<int, VergaderingVm>, IList, IBreadCrumb
    {
        private readonly VergaderingLogic _logic = new VergaderingLogic();
        private readonly DocTool _doc = new DocTool();

        // GET: Beheerder/Vergadering
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult List(int? jaar)
        {
            return PartialView("_vergaderingList", _logic.GetAllVergaderingen(jaar));
        }

        public PartialViewResult Create()
        {
            return PartialView("_createUpdate", new VergaderingVm { Datum = DateTime.Now});
        }

        public PartialViewResult Edit(int id)
        {
            return PartialView("_createUpdate", _logic.GetVergaderingById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUpdate(VergaderingVm model)
        {
            if (ModelState.IsValid)
            {
                _logic.AddOrUpdateVergadering(model);
                return Json(true);
            }

            return PartialView("_createUpdate", model);
        }

        [HttpPost]
        public ActionResult ToggleActive(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vergadering vergadering = _logic.ToggleActive(id);

            return Json(vergadering != null);
        }

        public PartialViewResult BreadCrumbs()
        {
            List<BreadCrumb> breadCrumbList = new List<BreadCrumb>();
            if (User.IsInRole("Admin") || User.IsInRole("Beheerder"))
            {
                breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/VergaderingBeheer", Beschrijving = "Vergadering-beheer" });
            }
            breadCrumbList.Add(new BreadCrumb { Url = "/Beheerder/Vergadering", Beschrijving = "Vergaderingen/verslagen" });    
                           
            ViewBag.Soort = "Vergadering";

            return PartialView("_breadCrumbs", breadCrumbList);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, int Id)
        {
            string fileName = _doc.BestandUploaden(file, Server);

            if (!string.IsNullOrEmpty(fileName))
            {
                _logic.AddNewDocsToDb(fileName, Id);
                return Json(true);
            }
            return Json(false);

        }

        [HttpPost]
        public ActionResult DeleteFile(int Id)
        {
            Verslag doc = _logic.GetFileById(Id);
            string bestandsnaam = doc.Bestandsnaam;

            bool gelukt = _doc.BestandVerwijderen(bestandsnaam, Server);
            if (!gelukt) return Json(false);
            _logic.RemoveDocFromDb(Id);
            return Json(true);
        }
    }
}