﻿using Gidsen.BLL.Logic;
using Gidsen.BOL.ViewModels;
using System.Web.Mvc;

namespace Beheersysteem.Areas.Beheerder.Controllers
{
    [Authorize(Roles = "Admin,Beheerder")]
    public class AlgemeenController : Controller
    {
        private readonly MenuLogic _logic = new MenuLogic();

        // GET: Beheerder/Algemeen
        public ActionResult Index()
        {
            return View(_logic.GetDynamicMenuItems(User, false));
        }
    }
}