﻿using System;
using System.Web.Mvc;
using Gidsen.BLL.Logic;
using Gidsen.BLL.Tools;

namespace Beheersysteem.Areas.Gids.Controllers
{
    public class AgendaController : Controller
    {
        private readonly AgendaLogic _logic = new AgendaLogic();

        // GET: Agenda
        [Authorize(Roles = "Admin, Beheerder, Gids, Stagiair")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Beheerder, Gids, Stagiair")]
        public ActionResult Openstaand()
        {
            return View();
        }
        
        [Authorize(Roles = "Admin, Beheerder, Gids, Stagiair")]
        public ActionResult Data(string from, string to)
        {
            return new JsonNetResult(_logic.GetCalendarData(DateTime.Parse(from), DateTime.Parse(to)));
            
        }

        [Authorize(Roles = "Admin, Beheerder, Gids, Stagiair")]
        public PartialViewResult GetOpenstaandeGidsbeurten()
        {
            return PartialView("_openstaandeGidsbeurten", _logic.GetAlleOpenstaande());
        }

        [Authorize]
        public PartialViewResult Details(int id)
        {
            return PartialView("_Details", _logic.GetGidsbeurtById(id));
        }

        [Authorize(Roles = "Beheerder, Gids, Stagiair")]
        [HttpPost]
        public ActionResult OpgevenAlsGids(int id, string gidsRol, string userId)
        {
            return Json(_logic.GidsToevoegen(id, gidsRol, userId, User));
        }

        [Authorize(Roles = "Beheerder, Gids, Stagiair")]
        [HttpPost]
        public ActionResult VerwijderGids(int gidsbeurtId, string userId)
        {
            _logic.GidsVerwijderen(gidsbeurtId, userId, Server);
            return Json(true);
        }

        [Authorize(Roles = "Admin, Beheerder, Gids, Stagiair")]
        [HttpPost]
        public PartialViewResult ResetUser(int gidsbeurtId, string userId)
        {
            return PartialView("_gidsenLijst", _logic.ResetAndReturnList(gidsbeurtId, userId, Server));
        }
    }   
}