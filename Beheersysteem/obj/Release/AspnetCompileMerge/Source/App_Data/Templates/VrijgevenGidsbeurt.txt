﻿ <!DOCTYPE html>
  <html>
 <head>  
    <meta charset="utf-8" />
    <title>Nieuwe beschikbare gidsbeurt</title>
</head>  

   <body>  
    	  
    <div style='width:600px; height:350px; padding-left:30px; padding-right:30px; margin-top:10px;'>

    <p style="font-family: Arial;font-size:16px;">Beste <strong>{0}</strong>,<p>

    <p style="font-family: Arial;font-size:16px;">Een gids die gekoppeld stond aan een gidsbeurt, heeft zonet aangegeven dat hij ze niet langer kan uitvoeren. De gidsbeurt is dus terug ter beschikking voor de andere gidsen.</p> 
	<p style="font-family: Arial;font-size:16px;">U ontvangt deze mail om u te verwittigen. De details kan u bekijken op het gidsen-platform</p> 
    <p style="font-family: Arial;font-size:16px;"><u>De gidsbeurt in kwestie is voorzien op</u>:</p>
    <p style="font-family: Arial;font-size:16px;">{1}</p>
    <p style="font-family: Arial;font-size:16px;">Vriendelijke groeten, <br><br><br>
	<em>De Olmense Zoo</em>
	</p>
	
    </div>
  </body>
 </html>