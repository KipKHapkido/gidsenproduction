﻿<!DOCTYPE html>
 <html>
<head>  
    <meta charset="utf-8" />
    <title>Reset paswoord</title>
</head>  

   <body>  
    	  
    <div style='width:600px; height:350px; padding-left:30px; padding-right:30px; margin-top:10px;'>

    <p style="font-family: Arial;font-size:16px;">Beste <strong>{0}</strong>,<p>

    <p style="font-family: Arial;font-size:16px;">Wij kregen een aanvraag om uw paswoord te resetten</p> 
    <p style="font-family: Arial;font-size:16px;"><u>Klik op onderstaande link om uw paswoord te resetten en verder te gaan</u>:</p>
    <p style="font-family: Arial;font-size:16px;">
		<a href='{1}' style='display: inline-block; text-align:center; vertical-align: middle; text-decoration:none; line-height: 1.42857143; padding: 8px 16px; font-size: 18px; color: #fff; background-color: #5cb85c; border-color: #4cae4c;border-radius: 4px;'>Reset Paswoord</a>
	</p>
	
	<p style="font-family: Arial;font-size:16px;"><strong>OF</strong></p>
	
	<p style="font-family: Arial;font-size:16px;">Negeer deze mail, indien u deze handeling niet hebt gesteld</p>
	  <p>&nbsp;</p> 
    <p style="font-family: Arial;font-size:16px;">Vriendelijke groeten, <br><br><br>
	<em>De Olmense Zoo</em>
	</p>
	
    </div>
  </body>
 </html>