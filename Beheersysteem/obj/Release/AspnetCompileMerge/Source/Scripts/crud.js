﻿$.ajax({
    url: controller + "BreadCrumbs",
    async: true,
    type: "GET",
    success: function (resp) {
        $("#breadcrumb").html(resp);
        $.fn.tooltips();
    }
});

$.fn.picker = function () {

    $("#Datum").datetimepicker({
        useCurrent: false,
        sideBySide: true,
        locale: "nl",
        stepping: 15,
        enabledHours: [10, 11, 12, 13, 14, 15, 16, 17]
    });

    if ($("#gidsbeurtId").val() == 0) {
        $("#Datum").data("DateTimePicker").minDate(moment({ hour: 10, minute: 0 }).add(14, "days"));
    } else if (!$("#gidsbeurtId").length) {
        $("#Datum").data("DateTimePicker").minDate(moment({ hour: 10, minute: 0 }).add(1, "days"));
    }

};

$.fn.setInit = function () {
    if ($("#Datum").length) {
        $.fn.picker();
    }
    if ($("#SteunProject").length) {
        $("#SteunProject").bootstrapToggle();
    }
    if ($("#Deelnemers").length) {

        $("#Rolstoel").bootstrapToggle();
        $("#EtenNaGidsbeurt").bootstrapToggle();
        $("#ShowNaGidsbeurt").bootstrapToggle();
        var slider = new Slider("#Deelnemers",
            {
                id: "slider1",
                min: 0,
                max: 20,
                tooltip_position: "right",
                formatter: function (value) {
                    return "Aantal: " + value;
                }
            });
    }
    if ($("#TopNavigatie").length) {
        $("#TopNavigatie").bootstrapToggle();
    }
};



$(document).on("click", ".toggle-active", function () {
    var dataString = "id=" + $(this).val();
    bootbox.confirm("Bent u zeker dat u dit item wil (de)activeren?", function (result) {
        if (result) {
            $.ajax({
                url: controller + "ToggleActive",
                async: true,
                data: dataString,
                type: "POST",
                success: function () {
                    $.fn.reload();

                    $("<div class='alert alert-success'>(de)activeren gelukt!</div>").insertBefore(".overzicht");
                }
            });
        }
    });
});

$(document).on("click", ".new", function (e) {
    e.preventDefault();

    $.ajax({
        url: controller + "Create",
        async: true,
        type: "GET",
        success: function (resp) {
            $("#myModal .modal-body").html(resp);
            $("#myModal .modal-title").html(titel + "aanmaken");
            $.fn.setInit();
            $("#submit").val(titel + "aanmaken");
            $("#myModal").modal("show");
            if ($("#Datum").length) {
                $('#myModal').on('shown.bs.modal', function () {
                        $("#Datum").focus();
                });
            }
            $.fn.removeMessage();
        }
    });
});

$(document).on("click", ".edit", function (e) {
    e.preventDefault();
    var dataString = "id=" + $(this).attr("data-value");

    $.ajax({
        url: controller + "Edit",
        async: true,
        data: dataString,
        type: "GET",
        success: function (resp) {
            $("#myModal .modal-body").html(resp);
            $("#myModal .modal-title").html(titel + "wijzigen");
            $("#submit").val(titel + "wijzigen");
            $.fn.setInit();
            $("#myModal").modal("show");

            $.fn.removeMessage();
        }
    });
});

$(document).on("submit", "#CreateUpdate", function (e) {
    e.preventDefault();
    var knop = $("#submit").val();
    var dataString = $("#CreateUpdate").serialize();
    $.ajax({
        url: controller + "CreateUpdate",
        async: true,
        data: dataString,
        type: "POST",
        success: function (resp) {
            if (resp === true) {
                $("#myModal").modal("hide");
                $.fn.reload();
                $("<div class='alert alert-success'>Toevoegen/Wijzigen gelukt!</div>").insertBefore(".overzicht");

            } else {
                $(".modal-body").html(resp);
                $("#submit").val(knop);
                $.fn.setInit();
                $.fn.removeMessage();
            }

        }
    });

});