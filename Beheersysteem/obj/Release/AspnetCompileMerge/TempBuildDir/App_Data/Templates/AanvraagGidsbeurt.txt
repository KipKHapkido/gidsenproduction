﻿ <!DOCTYPE html>
  <html>
 <head>  
    <meta charset="utf-8" />
    <title>Aanvraag gidsbeurt</title>
</head>  

   <body>  
    	  
    <div style='width:600px; height:350px; padding-left:30px; padding-right:30px; margin-top:10px;'>

    <p style="font-family: Arial;font-size:16px;">Beste <strong>{0}</strong>,<p>

    <p style="font-family: Arial;font-size:16px;">Wij kregen van een klant een aanvraag tot een nieuwe gidsbeurt.</p> 
	<p style="font-family: Arial;font-size:16px;">U ontvangt deze mail om u hiervan te verwittigen. De gidsbeurt is pas zichtbaar voor de gidsen, van zodra de aanvraag door één van de beheerders werd goedgekeurd.</p> 
    <p style="font-family: Arial;font-size:16px;"><u>De openstaande aanvraag is voorzien voor volgende datum/uur</u>:</p>
    <p style="font-family: Arial;font-size:16px;">{1}</p>
	  
    <p style="font-family: Arial;font-size:16px;">Vriendelijke groeten, <br><br><br>
	<em>De Olmense Zoo</em>
	</p>
	
    </div>
  </body>
 </html>