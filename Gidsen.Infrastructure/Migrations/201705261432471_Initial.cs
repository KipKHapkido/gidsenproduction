namespace Gidsen.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agendapunt",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Omschrijving = c.String(),
                        Actief = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                        Vergadering_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.Vergadering", t => t.Vergadering_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Vergadering_Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Familienaam = c.String(),
                        Voornaam = c.String(),
                        Actief = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Vergadering",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                        VerslagAanwezig = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                        Verslag_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Verslag", t => t.Verslag_Id)
                .Index(t => t.Verslag_Id);
            
            CreateTable(
                "dbo.Verslag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bestandsnaam = c.String(),
                        Afbeelding = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categorie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naam = c.String(),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dier",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naam = c.String(),
                        Actief = c.Boolean(nullable: false),
                        Categorie_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categorie", t => t.Categorie_Id)
                .Index(t => t.Categorie_Id);
            
            CreateTable(
                "dbo.DierDocumentatie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bestandsnaam = c.String(),
                        Afbeelding = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                        Dier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dier", t => t.Dier_Id)
                .Index(t => t.Dier_Id);
            
            CreateTable(
                "dbo.Gidsbeurt",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Text = c.String(),
                        Deelnemers = c.Byte(nullable: false),
                        Rolstoel = c.Boolean(nullable: false),
                        EtenNaGidsbeurt = c.Boolean(nullable: false),
                        ShowNaGidsbeurt = c.Boolean(nullable: false),
                        Inlichtingen = c.String(),
                        GidsToegewezen = c.Boolean(nullable: false),
                        Goedgekeurd = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                        LeeftijdCategorie_Id = c.Int(),
                        Thema_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LeeftijdCategorie", t => t.LeeftijdCategorie_Id)
                .ForeignKey("dbo.Thema", t => t.Thema_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.LeeftijdCategorie_Id)
                .Index(t => t.Thema_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.GidsbeurtUitvoerder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Actief = c.Boolean(nullable: false),
                        Gidsbeurt_Id = c.Int(),
                        GidsRol_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gidsbeurt", t => t.Gidsbeurt_Id)
                .ForeignKey("dbo.GidsRol", t => t.GidsRol_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.Gidsbeurt_Id)
                .Index(t => t.GidsRol_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.GidsRol",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Benaming = c.String(),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LeeftijdCategorie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Beschrijving = c.String(),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Thema",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naam = c.String(),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ThemaDocumentatie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bestandsnaam = c.String(),
                        Afbeelding = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                        Thema_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Thema", t => t.Thema_Id)
                .Index(t => t.Thema_Id);
            
            CreateTable(
                "dbo.Menu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Weergave = c.String(nullable: false),
                        Action = c.String(),
                        Controller = c.String(),
                        Area = c.String(),
                        TopNavigatie = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuRol",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Actief = c.Boolean(nullable: false),
                        IdentityRole_Id = c.String(maxLength: 128),
                        Menu_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.IdentityRole_Id)
                .ForeignKey("dbo.Menu", t => t.Menu_Id)
                .Index(t => t.IdentityRole_Id)
                .Index(t => t.Menu_Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SteunProject",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Jaartal = c.Int(nullable: false),
                        Steun = c.Boolean(nullable: false),
                        Actief = c.Boolean(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Vergoeding",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Jaar = c.Int(nullable: false),
                        Hoofdbedrag = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Steunproject = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Actief = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SteunProject", "User_Id", "dbo.User");
            DropForeignKey("dbo.MenuRol", "Menu_Id", "dbo.Menu");
            DropForeignKey("dbo.MenuRol", "IdentityRole_Id", "dbo.Roles");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Gidsbeurt", "User_Id", "dbo.User");
            DropForeignKey("dbo.Gidsbeurt", "Thema_Id", "dbo.Thema");
            DropForeignKey("dbo.ThemaDocumentatie", "Thema_Id", "dbo.Thema");
            DropForeignKey("dbo.Gidsbeurt", "LeeftijdCategorie_Id", "dbo.LeeftijdCategorie");
            DropForeignKey("dbo.GidsbeurtUitvoerder", "User_Id", "dbo.User");
            DropForeignKey("dbo.GidsbeurtUitvoerder", "GidsRol_Id", "dbo.GidsRol");
            DropForeignKey("dbo.GidsbeurtUitvoerder", "Gidsbeurt_Id", "dbo.Gidsbeurt");
            DropForeignKey("dbo.DierDocumentatie", "Dier_Id", "dbo.Dier");
            DropForeignKey("dbo.Dier", "Categorie_Id", "dbo.Categorie");
            DropForeignKey("dbo.Vergadering", "Verslag_Id", "dbo.Verslag");
            DropForeignKey("dbo.Agendapunt", "Vergadering_Id", "dbo.Vergadering");
            DropForeignKey("dbo.Agendapunt", "User_Id", "dbo.User");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropIndex("dbo.SteunProject", new[] { "User_Id" });
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.MenuRol", new[] { "Menu_Id" });
            DropIndex("dbo.MenuRol", new[] { "IdentityRole_Id" });
            DropIndex("dbo.ThemaDocumentatie", new[] { "Thema_Id" });
            DropIndex("dbo.GidsbeurtUitvoerder", new[] { "User_Id" });
            DropIndex("dbo.GidsbeurtUitvoerder", new[] { "GidsRol_Id" });
            DropIndex("dbo.GidsbeurtUitvoerder", new[] { "Gidsbeurt_Id" });
            DropIndex("dbo.Gidsbeurt", new[] { "User_Id" });
            DropIndex("dbo.Gidsbeurt", new[] { "Thema_Id" });
            DropIndex("dbo.Gidsbeurt", new[] { "LeeftijdCategorie_Id" });
            DropIndex("dbo.DierDocumentatie", new[] { "Dier_Id" });
            DropIndex("dbo.Dier", new[] { "Categorie_Id" });
            DropIndex("dbo.Vergadering", new[] { "Verslag_Id" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.Agendapunt", new[] { "Vergadering_Id" });
            DropIndex("dbo.Agendapunt", new[] { "User_Id" });
            DropTable("dbo.Vergoeding");
            DropTable("dbo.SteunProject");
            DropTable("dbo.Roles");
            DropTable("dbo.MenuRol");
            DropTable("dbo.Menu");
            DropTable("dbo.ThemaDocumentatie");
            DropTable("dbo.Thema");
            DropTable("dbo.LeeftijdCategorie");
            DropTable("dbo.GidsRol");
            DropTable("dbo.GidsbeurtUitvoerder");
            DropTable("dbo.Gidsbeurt");
            DropTable("dbo.DierDocumentatie");
            DropTable("dbo.Dier");
            DropTable("dbo.Categorie");
            DropTable("dbo.Verslag");
            DropTable("dbo.Vergadering");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.Agendapunt");
        }
    }
}
