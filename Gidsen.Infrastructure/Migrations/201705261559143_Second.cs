namespace Gidsen.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Agendapunt", "Omschrijving", c => c.String(unicode: false));
            AlterColumn("dbo.User", "Familienaam", c => c.String(unicode: false));
            AlterColumn("dbo.User", "Voornaam", c => c.String(unicode: false));
            AlterColumn("dbo.User", "PasswordHash", c => c.String(unicode: false));
            AlterColumn("dbo.User", "SecurityStamp", c => c.String(unicode: false));
            AlterColumn("dbo.User", "PhoneNumber", c => c.String(unicode: false));
            AlterColumn("dbo.User", "LockoutEndDateUtc", c => c.DateTime(precision: 0));
            AlterColumn("dbo.UserClaim", "ClaimType", c => c.String(unicode: false));
            AlterColumn("dbo.UserClaim", "ClaimValue", c => c.String(unicode: false));
            AlterColumn("dbo.Vergadering", "Datum", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Verslag", "Bestandsnaam", c => c.String(unicode: false));
            AlterColumn("dbo.Categorie", "Naam", c => c.String(unicode: false));
            AlterColumn("dbo.Dier", "Naam", c => c.String(unicode: false));
            AlterColumn("dbo.DierDocumentatie", "Bestandsnaam", c => c.String(unicode: false));
            AlterColumn("dbo.Gidsbeurt", "StartDate", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Gidsbeurt", "EndDate", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Gidsbeurt", "Text", c => c.String(unicode: false));
            AlterColumn("dbo.Gidsbeurt", "Inlichtingen", c => c.String(unicode: false));
            AlterColumn("dbo.GidsRol", "Benaming", c => c.String(unicode: false));
            AlterColumn("dbo.LeeftijdCategorie", "Beschrijving", c => c.String(unicode: false));
            AlterColumn("dbo.Thema", "Naam", c => c.String(unicode: false));
            AlterColumn("dbo.ThemaDocumentatie", "Bestandsnaam", c => c.String(unicode: false));
            AlterColumn("dbo.Menu", "Weergave", c => c.String(nullable: false, unicode: false));
            AlterColumn("dbo.Menu", "Action", c => c.String(unicode: false));
            AlterColumn("dbo.Menu", "Controller", c => c.String(unicode: false));
            AlterColumn("dbo.Menu", "Area", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Menu", "Area", c => c.String());
            AlterColumn("dbo.Menu", "Controller", c => c.String());
            AlterColumn("dbo.Menu", "Action", c => c.String());
            AlterColumn("dbo.Menu", "Weergave", c => c.String(nullable: false));
            AlterColumn("dbo.ThemaDocumentatie", "Bestandsnaam", c => c.String());
            AlterColumn("dbo.Thema", "Naam", c => c.String());
            AlterColumn("dbo.LeeftijdCategorie", "Beschrijving", c => c.String());
            AlterColumn("dbo.GidsRol", "Benaming", c => c.String());
            AlterColumn("dbo.Gidsbeurt", "Inlichtingen", c => c.String());
            AlterColumn("dbo.Gidsbeurt", "Text", c => c.String());
            AlterColumn("dbo.Gidsbeurt", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Gidsbeurt", "StartDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DierDocumentatie", "Bestandsnaam", c => c.String());
            AlterColumn("dbo.Dier", "Naam", c => c.String());
            AlterColumn("dbo.Categorie", "Naam", c => c.String());
            AlterColumn("dbo.Verslag", "Bestandsnaam", c => c.String());
            AlterColumn("dbo.Vergadering", "Datum", c => c.DateTime(nullable: false));
            AlterColumn("dbo.UserClaim", "ClaimValue", c => c.String());
            AlterColumn("dbo.UserClaim", "ClaimType", c => c.String());
            AlterColumn("dbo.User", "LockoutEndDateUtc", c => c.DateTime());
            AlterColumn("dbo.User", "PhoneNumber", c => c.String());
            AlterColumn("dbo.User", "SecurityStamp", c => c.String());
            AlterColumn("dbo.User", "PasswordHash", c => c.String());
            AlterColumn("dbo.User", "Voornaam", c => c.String());
            AlterColumn("dbo.User", "Familienaam", c => c.String());
            AlterColumn("dbo.Agendapunt", "Omschrijving", c => c.String());
        }
    }
}
