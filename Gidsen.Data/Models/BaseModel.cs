﻿using System.ComponentModel;

namespace Gidsen.BOL.Models
{
    public abstract class Zeus
    {
        public int Id { get; set; }

        [DefaultValue("true")]
        public bool Actief { get; set; }

        protected Zeus()
        {
            Actief = true;
        }
    }

    public class Documentatie : Zeus
    {
        public string Bestandsnaam { get; set; }

        public bool Afbeelding { get; set; }

        
    }
}