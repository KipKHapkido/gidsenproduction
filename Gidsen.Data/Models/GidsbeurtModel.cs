﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.Models
{
    public class Gidsbeurt: Zeus
    {
        [Display(Name = "Datum & Tijdstip")]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Display(Name = "Groepsbenaming")]
        public string Text { get; set; }

        [Display(Name = "Aantal deelnemers")]
        public byte Deelnemers { get; set; }

        public LeeftijdCategorie LeeftijdCategorie { get; set; }

        public Thema Thema { get; set; }

        [Display(Name = "Rolstoelpatiënten?")]
        public bool Rolstoel { get; set; }

        [Display(Name = "Aansluitend eten?")]
        public bool EtenNaGidsbeurt { get; set; }

        [Display(Name = "Aansluitend show?")]
        public bool ShowNaGidsbeurt { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Wensen/inlichtingen?")]
        public string Inlichtingen { get; set; }

        public virtual ApplicationUser User { get; set; }

        public List<GidsbeurtUitvoerder> GidsbeurtUitvoerder { get; set; }

        public bool GidsToegewezen { get; set; }

        public bool Goedgekeurd { get; set; }
    }

    public class LeeftijdCategorie: Zeus
    {
        public string Beschrijving { get; set; }
    }

    public class GidsbeurtUitvoerder: Zeus
    {
        public virtual ApplicationUser User { get; set; }

        public GidsRol GidsRol { get; set; }

        public Gidsbeurt Gidsbeurt { get; set; }

    }

    public class GidsRol: Zeus
    {
       public string Benaming { get; set; }
    }

    public class Thema: Zeus
    {
        public string Naam { get; set; }

        public List<ThemaDocumentatie> ThemaDocumentatie { get; set; }
    }

    public class ThemaDocumentatie: Documentatie
    {
        public Thema Thema { get; set; }
    }
}