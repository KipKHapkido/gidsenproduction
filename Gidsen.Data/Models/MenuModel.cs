﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.Models
{
    public class Menu: Zeus
    {
        [Required]
        public string Weergave { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }

        public string Area { get; set; }

        public bool TopNavigatie { get; set; }

        public List<MenuRol> MenuRol { get; set; }
    }

    public class MenuRol: Zeus
    {
        public Menu Menu { get; set; }

        public IdentityRole IdentityRole { get; set; }

    }
}
