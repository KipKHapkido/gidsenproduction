﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gidsen.BOL.ViewModels
{
    public class SteunVm: BaseVm
    {
        public bool Steun { get; set; }
    }
}
