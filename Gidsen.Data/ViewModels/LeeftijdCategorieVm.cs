﻿using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class LeeftijdCategorieVm: BaseVm
    {
        [Required(ErrorMessage = "Beschrijving is een verplicht veld!")]
        public string Beschrijving { get; set; }
    }

    public class LeeftijdCategorieListVm : BaseVm
    {
        public List<LeeftijdCategorie> LijstLeeftijdCategorie { get; set; }
    }
}
