﻿using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class VergoedingVm: BaseVm
    {
        [Required(ErrorMessage = "Jaar is een verplicht veld!")]
        [Display(Name = "Welk jaar?")]
        public int Jaar { get; set; }
        
        [Required(ErrorMessage = "Hoofdbedrag is een verplicht veld!")]
        [Display(Name = "Bedrag volledige vergoeding?")]
        public decimal Hoofdbedrag { get; set; }
                
        [Display(Name = "Aandeel steunproject?")]
        public decimal Steunproject { get; set; }
    }

    public class VergoedingListVm : BaseVm
    {
        public List<Vergoeding> LijstVergoedingen { get; set; }
    }
}
