﻿using System.Collections.Generic;
using Gidsen.BOL.Models;

namespace Gidsen.BOL.ViewModels
{
    public class UitkeringListVm: BaseVm
    {
        public List<Uitkering> UitkeringLijst { get; set; }

        public Vergoeding Vergoeding { get; set; }
    }
}
