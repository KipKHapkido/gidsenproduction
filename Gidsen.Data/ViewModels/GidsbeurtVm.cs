﻿using Gidsen.BOL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class GidsbeurtVm: BaseVm
    {
        public bool GidsToegewezen { get; set; }

        public bool Goedgekeurd { get; set; }

        [Required]
        [Display(Name = "Datum & Tijdstip")]
        public DateTime Datum { get; set; }

        [Required]
        [Display(Name = "Groepsbenaming")]
        public string Text { get; set; }

        [Required]
        [Range(1, 20,
            ErrorMessage = "Er moet minstens één deelnemer zijn")]
        [Display(Name = "Aantal deelnemers")]
        public byte Deelnemers { get; set; }

        [Required(ErrorMessage = "Leeftijd is een verplicht veld")]
        [Display(Name = "Gemiddelde Leeftijd?")]
        public int LeeftijdCategorieId { get; set; }

        public List<LeeftijdCategorie> ListLeeftijden { get; set; }

        [Required(ErrorMessage = "Thema is een verplicht veld")]
        [Display(Name = "Welk thema?")]
        public int ThemaId { get; set; }

        public List<Thema> ThemaLijst { get; set; }

        public bool Rolstoel { get; set; }

        public bool EtenNaGidsbeurt { get; set; }

        public bool ShowNaGidsbeurt { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Bijkomende wensen/inlichtingen?")]
        public string Inlichtingen { get; set; }

        public string UserId { get; set; }

        public List<ApplicationUser> KlantenLijst { get; set; }

    }

    public class GidsbeurtListVm : BaseVm
    {
        public List<Gidsbeurt> GidsbeurtLijst { get; set; }

        public Vergoeding Vergoeding { get; set; }

        public bool Steunproject { get; set; }

        public GidsbeurtListVm()
        {
            GidsbeurtLijst = new List<Gidsbeurt>();
        }
    }
}
