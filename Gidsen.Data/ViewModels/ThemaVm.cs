﻿using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gidsen.BOL.ViewModels
{
    public class ThemaVm: BaseVm
    {
        [Required(ErrorMessage = "Naam is een verplicht veld!")]
        public string Naam { get; set; }
    }

    public class ThemaListVm : BaseVm
    {
        public List<Thema> ThemaLijst { get; set; }
    }
}
