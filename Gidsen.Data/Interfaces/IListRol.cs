﻿using System.Web.Mvc;

namespace Gidsen.BOL.Interfaces
{
    public interface IListRol
    {
        [HttpGet]
        PartialViewResult List(string rol);
    }
}
