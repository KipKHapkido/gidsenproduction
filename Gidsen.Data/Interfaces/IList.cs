﻿using System.Web.Mvc;

namespace Gidsen.BOL.Interfaces
{
    public interface IList
    {
        [HttpGet]
        PartialViewResult List(int? keuze);
    }
}
