﻿using System.Web.Mvc;

namespace Gidsen.BOL.Interfaces
{
    public interface ICrudGeneric<in T, in TU>
    {                
        [HttpGet]
        PartialViewResult Create();

        [HttpGet]
        PartialViewResult Edit(T id);

        [HttpPost]
        [ValidateAntiForgeryToken]
        ActionResult CreateUpdate(TU model);

        [HttpPost]
        ActionResult ToggleActive(T id);   
    }
}
