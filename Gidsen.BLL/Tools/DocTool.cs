﻿using System;
using System.IO;
using System.Web;


namespace Gidsen.BLL.Tools
{
    public class DocTool
    {       
        public string BestandUploaden(HttpPostedFileBase file, HttpServerUtilityBase server)
        {
            if (file == null || file.ContentLength <= 0) return null;
            var fileName = Path.GetFileName(file.FileName);
            var finalFileName = DateTime.Now.ToString("yyyyMMdd") + "_" + fileName;
            var path = Path.Combine(server.MapPath("~/Documentatie/"), finalFileName);
            if (!File.Exists(path))
            {
                file.SaveAs(path);
            }
            return finalFileName;
        }

        public bool BestandVerwijderen(string bestandsnaam, HttpServerUtilityBase server)
        {
            var fileName = Path.GetFileName(bestandsnaam);
            if (fileName == null) return false;
            var path = Path.Combine(server.MapPath("~/Documentatie/"), fileName);
            if (!File.Exists(path)) return false;
            File.Delete(path);
            return true;
        }
    }
}
