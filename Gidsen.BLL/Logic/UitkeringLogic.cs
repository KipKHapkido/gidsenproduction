﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;

namespace Gidsen.BLL.Logic
{
    public class UitkeringLogic : BaseLogic
    {
        private readonly UitkeringListVm _ulVm;

        public UitkeringLogic()
        {
            _ulVm = new UitkeringListVm();
        }

        public UitkeringListVm GetAlleUitkeringen(int? jaar)
        {
            Vergoeding vergoeding = Db.Vergoeding.Where(x => x.Jaar <= jaar && x.Actief).OrderByDescending(x => x.Jaar).Take(1).SingleOrDefault();

            if (vergoeding != null)
            {
                _ulVm.Vergoeding = vergoeding;
            }
            else
            {
                _ulVm.AddMessage("Er werd nog geen vergoeding vastgelegd!", Message.MessageSeverity.Error);
                return _ulVm;
            }

            if (jaar != null)
            {
                List<string> roleIds = Db.Roles.Where(x => x.Name.Equals("Stagiair") || x.Name.Equals("Gids")).Select(x => x.Id).ToList();

                List<ApplicationUser> gidsenLijst = Db.Users.Where(r => roleIds.Contains(r.Roles.FirstOrDefault().RoleId) && r.Actief).OrderBy(x => x.Familienaam).ThenBy(x => x.Voornaam).ToList();

                _ulVm.UitkeringLijst = new List<Uitkering>();

                if (gidsenLijst.Any())
                {
                    foreach (var gids in gidsenLijst)
                    {

                        var gidsbeurtUitvoerdersList = Db.GidsbeurtUitvoerder.Include(x => x.GidsRol).Include(x => x.Gidsbeurt).Where(x => x.User.Id.Equals(gids.Id) && x.GidsRol.Benaming.Equals("Uitvoerder") && x.Gidsbeurt.StartDate.Year == jaar && x.Gidsbeurt.StartDate <= DateTime.Now && x.Gidsbeurt.Actief).ToList();

                        int aantalUitgevoerd = gidsbeurtUitvoerdersList.Select(x => x.Gidsbeurt).Distinct().Count();

                        SteunProject steunProject = Db.SteunProject.Where(x => x.Jaartal <= jaar && x.User.Id.Equals(gids.Id)).OrderByDescending(x => x.Jaartal).Take(1).SingleOrDefault();

                        bool steun = false;

                        if (steunProject != null)
                        {
                            steun = steunProject.Steun;
                        }

                        _ulVm.UitkeringLijst.Add(new Uitkering
                        {
                            Gids = gids,
                            UitgevoerdeGidsbeurten = aantalUitgevoerd,
                            Steunproject = steun
                        });
                    }
                }
                else
                {
                    _ulVm.AddMessage("Er werden nog geen gidsen teruggevonden!", Message.MessageSeverity.Error);
                }

            }
            else
            {
                _ulVm.AddMessage("U moet eerst een geldig jaar kiezen!", Message.MessageSeverity.Error);
            }

            return _ulVm;
        }

        public UitkeringListVm GetAllStagiairs()
        {
            string rolId = Db.Roles.Where(x => x.Name.Equals("Stagiair")).Select(x => x.Id).FirstOrDefault();
            List<ApplicationUser> gidsenLijst = Db.Users
                .Include(x => x.Roles)
                .Where(x => x.Roles.FirstOrDefault().RoleId.Equals(rolId)).OrderBy(x => x.Familienaam).ThenBy(x => x.Voornaam).ToList();


            _ulVm.UitkeringLijst = new List<Uitkering>();

            if (gidsenLijst.Any())
            {
                foreach (var gids in gidsenLijst)
                {
                    int aantalUitgevoerd = Db.GidsbeurtUitvoerder.Include(x => x.GidsRol).Count(x => x.User.Id.Equals(gids.Id) && x.GidsRol.Benaming.Equals("Uitvoerder") && x.Gidsbeurt.StartDate <= DateTime.Now);

                    int aantalGevolgd = Db.GidsbeurtUitvoerder.Include(x => x.GidsRol).Count(x => x.User.Id.Equals(gids.Id) && x.GidsRol.Benaming.Equals("Volger") && x.Gidsbeurt.StartDate <= DateTime.Now);

                    _ulVm.UitkeringLijst.Add(new Uitkering
                    {
                        Gids = gids,
                        UitgevoerdeGidsbeurten = aantalUitgevoerd,
                        GevolgdeGidsbeurten = aantalGevolgd
                    });
                }
            }
            else
            {
                _ulVm.AddMessage("Er werden geen stagiairs teruggevonden!", Message.MessageSeverity.Error);
            }

            return _ulVm;
        }
    }
}
