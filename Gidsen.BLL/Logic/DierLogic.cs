﻿using Gidsen.BOL.ViewModels;
using Gidsen.BOL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Gidsen.BLL.Logic
{
    public class DierLogic : BaseLogic
    {
        private DierVm _dVm;
        private readonly DierLijstVm _dlVm;

        public DierLogic()
        {
            _dVm = new DierVm();
            _dlVm = new DierLijstVm();
        }

        public DierVm GetAllActiveCategories()
        {
            List<Categorie> categorieLijst = Db.Categorie.Where(x => x.Actief).ToList();
            if (categorieLijst.Count > 0)
            {
                _dVm.CategorieLijst = categorieLijst;
            }
            else
            {
                _dVm.AddMessage("Er werden geen categorieën teruggevonden. U moet eerst een categorie toevoegen, vooraleer u er een dier aan kan koppelen!", Message.MessageSeverity.Error);
            }
            return _dVm;
        }

        public DierLijstVm GetAllDieren()
        {
            List<Dier> lijstDieren = Db.Dier.Include("Categorie").OrderBy(x => x.Naam).ToList();
            _dlVm.DierLijst = lijstDieren;
            return _dlVm;
        }

        public DierVm GetDierById(int id)
        {
            _dVm = GetAllActiveCategories();
            Dier dier = Db.Dier.Include(x => x.Categorie).SingleOrDefault(x => x.Id == id);
            if (dier == null) return _dVm;
            _dVm.Id = dier.Id;
            _dVm.Naam = dier.Naam;
            _dVm.Categorie = dier.Categorie;
            return _dVm;
        }

        public void AddOrUpdateDier(DierVm model)
        {
            Dier dier = new Dier
            {
                Id = model.Id,
                Naam = model.Naam,
                Categorie = Db.Categorie.Find(model.CategorieId)
            };

            Db.Entry(dier).State = dier.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }

        public Dier ToggleActive(int id)
        {
            Dier dier = Db.Dier.SingleOrDefault(x => x.Id == id);
            if (dier == null) return null;
            dier.Actief = !dier.Actief;
            Db.Entry(dier).State = EntityState.Modified;
            Db.SaveChanges();
            return dier;
        }
    }
}
