﻿using Gidsen.BOL.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Gidsen.BOL.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Gidsen.BLL.Tools;
using System;
using System.Web;

namespace Gidsen.BLL.Logic
{
    public class GebruikersLogic : BaseLogic
    {
        private CompleteUserVm _cuVm;
        private CompleteUserListVm _culVm;
        public SteunVm SVm;
        private readonly MailTool _tool;


        public GebruikersLogic()
        {
            _cuVm = new CompleteUserVm();
            _culVm = new CompleteUserListVm();
            SVm = new SteunVm();
            _tool = new MailTool();
        }

        public CompleteUserListVm GetAllUsers(string rolId)
        {
            if (!string.IsNullOrEmpty(rolId))
            {
                List<ApplicationUser> gebruikersLijst = Db.Users
                    .Include(x => x.Roles)
                    .Where(x => x.Roles.FirstOrDefault().RoleId.Equals(rolId))
                    .OrderBy(x => x.UserName)
                    .ToList();

                if (gebruikersLijst.Any())
                {
                    _culVm.UserList = gebruikersLijst;

                    _culVm.Roles = Db.Roles.ToList();
                }
                else
                {
                    _culVm.AddMessage("Er ging iets mis. Er werden geen gebruikers teruggevonden", Message.MessageSeverity.Error);
                }
            }
            else
            {
                _culVm.AddMessage("U moet een geldige gebruikersrol selecteren om resultaten te krijgen!", Message.MessageSeverity.Error);
            }
            return _culVm;
        }

        public bool AddOrUpdateGebruikerAsync(CompleteUserVm model, HttpServerUtilityBase server)
        {
            if (string.IsNullOrEmpty(model.UserId))
            {
                using (var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Db)))
                {
                    var newUser = new ApplicationUser() { UserName = model.Email, Email = model.Email, Familienaam = model.Familienaam, Voornaam = model.Voornaam, PhoneNumber = model.PhoneNumber };

                    string userPwd = GeneratePassword() ?? "Azerty123!";

                    var result = userManager.Create(newUser, userPwd);

                    if (result.Succeeded)
                    {
                        var user = Db.Users.FirstOrDefault(x => x.Email.Equals(model.Email));

                        if (user != null)
                        {
                            user.UserName = model.UserName;
                            user.EmailConfirmed = true;

                            Db.Entry(user).State = EntityState.Modified;
                            Db.SaveChanges();

                            if (!string.IsNullOrEmpty(model.RoleId))
                            {
                                IdentityRole rol = Db.Roles.FirstOrDefault(x => x.Id.Equals(model.RoleId));
                                if (rol == null) throw new ArgumentNullException(nameof(rol));
                                userManager.AddToRole(user.Id, rol.Name);
                                Db.SaveChanges();
                            }

                            _tool.GmailMessage(user, userPwd, MailTool.EmailSoort.AccountGegevens, DateTime.Now,
                                server);
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            else
            {
                _culVm = SaveCompleteUserData(model);
                return true;
            }
        }

        private string GeneratePassword()
        {
            var validator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            bool requireNonLetterOrDigit = validator.RequireNonLetterOrDigit;
            bool requireDigit = validator.RequireDigit;
            bool requireLowercase = validator.RequireLowercase;
            bool requireUppercase = validator.RequireUppercase;

            string randomPassword = string.Empty;

            int passwordLength = validator.RequiredLength;

            Random random = new Random();
            while (randomPassword.Length != passwordLength)
            {
                int randomNumber = random.Next(48, 122);  // >= 48 && < 122 
                if (randomNumber == 95 || randomNumber == 96) continue;  // != 95, 96 _'

                char c = Convert.ToChar(randomNumber);

                if (requireDigit)
                    if (char.IsDigit(c))
                        requireDigit = false;

                if (requireLowercase)
                    if (char.IsLower(c))
                        requireLowercase = false;

                if (requireUppercase)
                    if (char.IsUpper(c))
                        requireUppercase = false;

                if (requireNonLetterOrDigit)
                    if (!char.IsLetterOrDigit(c))
                        requireNonLetterOrDigit = false;

                randomPassword += c;
            }

            if (requireDigit)
                randomPassword += Convert.ToChar(random.Next(48, 58));  // 0-9

            if (requireLowercase)
                randomPassword += Convert.ToChar(random.Next(97, 123));  // a-z

            if (requireLowercase)
                randomPassword += Convert.ToChar(random.Next(65, 91));  // A-Z

            if (requireNonLetterOrDigit)
                randomPassword += Convert.ToChar(random.Next(33, 48));  // symbols !"#$%&'()*+,-./

            return randomPassword;
        }

        public ApplicationUser ToggleActive(string id)
        {
            ApplicationUser gebruiker = Db.Users.SingleOrDefault(x => x.Id.Equals(id));
            if (gebruiker == null) return null;

            gebruiker.Actief = !gebruiker.Actief;
            Db.Entry(gebruiker).State = EntityState.Modified;
            Db.SaveChanges();
            return gebruiker;
        }

        public CompleteUserListVm GetUserData(IPrincipal user)
        {
            string userId = user.Identity.GetUserId();
            return GetUserById(userId);
        }

        public CompleteUserListVm GetUserById(string userId)
        {
            ApplicationUser gebruiker = Db.Users.Include(x => x.Roles).SingleOrDefault(x => x.Id.Equals(userId));
            if (gebruiker != null)
            {
                _culVm.UserList.Add(gebruiker);
                _culVm.Roles = Db.Roles.ToList();

            }
            else
            {
                _culVm.AddMessage("Er ging iets mis. De gebruikersgegevens werden niet teruggevonden", Message.MessageSeverity.Error);
            }

            return _culVm;
        }

        public CompleteUserListVm SaveCompleteUserData(CompleteUserVm model)
        {
            ApplicationUser aangepasteGebruiker = Db.Users
                .Include(x => x.Roles)
                .FirstOrDefault(x => x.Id.Equals(model.UserId));

            if (aangepasteGebruiker != null)
            {
                aangepasteGebruiker.UserName = model.UserName;
                aangepasteGebruiker.Familienaam = model.Familienaam;
                aangepasteGebruiker.Voornaam = model.Voornaam;
                aangepasteGebruiker.PhoneNumber = model.PhoneNumber;
                aangepasteGebruiker.Email = model.Email;
                aangepasteGebruiker.EmailConfirmed = true;

                Db.Entry(aangepasteGebruiker).State = EntityState.Modified;
                Db.SaveChanges();

                var identityUserRole = aangepasteGebruiker.Roles.FirstOrDefault();
                if (identityUserRole != null && (!string.IsNullOrEmpty(model.RoleId) && (model.RoleId != identityUserRole.RoleId)))
                {

                    Db.UserRole.Remove(identityUserRole);
                    Db.SaveChanges();

                    IdentityUserRole nieuweRol = new IdentityUserRole
                    {
                        RoleId = model.RoleId,
                        UserId = aangepasteGebruiker.Id
                    };

                    Db.UserRole.Add(nieuweRol);
                    Db.SaveChanges();

                }

                if (model.HuidigeRolnaam.Equals("Gids") || model.HuidigeRolnaam.Equals("Stagiair"))
                {
                    SteunProject steunProject = Db.SteunProject.FirstOrDefault(x => x.Jaartal == DateTime.Now.Year && x.User.Id.Equals(aangepasteGebruiker.Id));

                    if (steunProject != null)
                    {
                        steunProject.Steun = model.SteunProject;
                        Db.Entry(steunProject).State = EntityState.Modified;
                    }
                    else
                    {
                        steunProject = new SteunProject
                        {
                            Jaartal = DateTime.Now.Year,
                            Steun = model.SteunProject,
                            User = aangepasteGebruiker
                        };
                        Db.Entry(steunProject).State = EntityState.Added;
                    }
                    Db.SaveChanges();

                }

                _culVm.AddMessage("Gebruiker " + aangepasteGebruiker.UserName + " werd aangepast!", Message.MessageSeverity.Success);

            }
            else
            {
                _culVm.AddMessage("Er ging iets mis. De gebruiker kon niet aangepast worden!", Message.MessageSeverity.Error);
            }
            if (aangepasteGebruiker != null) return GetUserById(aangepasteGebruiker.Id);
            else return _culVm;
        }

        public CompleteUserVm GetUserDataById(string id, IPrincipal user)
        {
            ApplicationUser gebruiker = Db.Users.Include(x => x.Roles).SingleOrDefault(x => x.Id.Equals(id));
            if (gebruiker != null)
            {
                _cuVm.UserId = gebruiker.Id;
                _cuVm.UserName = gebruiker.UserName;
                _cuVm.Familienaam = gebruiker.Familienaam;
                _cuVm.Voornaam = gebruiker.Voornaam;
                _cuVm.Email = gebruiker.Email;
                _cuVm.PhoneNumber = gebruiker.PhoneNumber;
                _cuVm = GetRoles(user);
                var identityUserRole = gebruiker.Roles.FirstOrDefault();
                if (identityUserRole != null) _cuVm.RoleId = identityUserRole.RoleId;
                _cuVm.HuidigeRolnaam = Db.Roles.Where(x => x.Id.Equals(_cuVm.RoleId)).Select(x => x.Name).FirstOrDefault();

                if (_cuVm.HuidigeRolnaam != null && (_cuVm.HuidigeRolnaam.Equals("Gids") || _cuVm.HuidigeRolnaam.Equals("Stagiair")))
                {
                    SteunProject steunProject = Db.SteunProject.Where(x => x.Jaartal <= DateTime.Now.Year && x.User.Id.Equals(gebruiker.Id)).OrderByDescending(x => x.Jaartal).Take(1).FirstOrDefault();

                    _cuVm.SteunProject = false;
                    if (steunProject != null)
                        _cuVm.SteunProject = steunProject.Steun;
                }
            }
            else
            {
                _culVm.AddMessage("Er ging iets mis. De gebruikersgegevens werden niet teruggevonden", Message.MessageSeverity.Error);
            }
            return _cuVm;
        }

        public CompleteUserVm GetRoles(IPrincipal user)
        {
            _cuVm.Roles = user.IsInRole("Admin") ? Db.Roles.ToList() : Db.Roles.Where(x => !x.Name.Equals("Admin")).ToList();
            _cuVm.HuidigeRolnaam = "";

            return _cuVm;
        }

        public SteunVm GetSteun(string gebruikerId)
        {
            SteunProject steunProject = Db.SteunProject.Where(x => x.Jaartal <= DateTime.Now.Year && x.User.Id.Equals(gebruikerId)).OrderByDescending(x => x.Jaartal).Take(1).FirstOrDefault();
            if (steunProject != null)
                SVm.Steun = steunProject.Steun;
            else
                SVm.Steun = false;
            return SVm;
        }
    }
}
