﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Gidsen.BOL.Models;
using Gidsen.BOL.ViewModels;

namespace Gidsen.BLL.Logic
{
    public class LeeftijdCategorieLogic : BaseLogic
    {
        private readonly LeeftijdCategorieListVm _lclVm;
        private readonly LeeftijdCategorieVm _lcVm;

        public LeeftijdCategorieLogic()
        {
            _lclVm = new LeeftijdCategorieListVm();
            _lcVm = new LeeftijdCategorieVm();
        }

        public LeeftijdCategorieListVm GetAllLeeftijdCategorie()
        {
            List<LeeftijdCategorie> leeftijdCategorieList = Db.LeeftijdCategorie.ToList();
            if (leeftijdCategorieList.Any())
            {
                _lclVm.LijstLeeftijdCategorie = leeftijdCategorieList;
            }
            else
            {
                _lclVm.AddMessage("Er werden nog geen leeftijdscategorieën gevonden", Message.MessageSeverity.Error);
            }

            return _lclVm;
        }

        public LeeftijdCategorieVm GetLeeftijdCategorieById(int id)
        {
            LeeftijdCategorie leeftijdCategorie = Db.LeeftijdCategorie.FirstOrDefault(x => x.Id == id);
            if (leeftijdCategorie != null)
            {
                _lcVm.Id = leeftijdCategorie.Id;
                _lcVm.Beschrijving = leeftijdCategorie.Beschrijving;
            } 
            else
            {
                _lcVm.AddMessage("Er ging iets mis. De gevraagde leeftijdcategorie werd niet teruggevonden", Message.MessageSeverity.Error);
            }
            return _lcVm;
        }

        public void AddOrUpdateLeeftijdCategorie(LeeftijdCategorieVm model)
        {
            LeeftijdCategorie leeftijdCategorie = new LeeftijdCategorie
            {
                Id = model.Id,
                Beschrijving = model.Beschrijving
            };

            Db.Entry(leeftijdCategorie).State = leeftijdCategorie.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;

            Db.SaveChanges();
        }
                
        public LeeftijdCategorie ToggleActive(int id)
        {
            LeeftijdCategorie leeftijdCategorie = Db.LeeftijdCategorie.FirstOrDefault(x => x.Id == id);
            if (leeftijdCategorie == null) return null;

            leeftijdCategorie.Actief = !leeftijdCategorie.Actief;
            Db.Entry(leeftijdCategorie).State = EntityState.Modified;
            Db.SaveChanges();
            return leeftijdCategorie;
        }
                
    }
}
